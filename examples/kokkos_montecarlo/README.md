# UPC++/Kokkos Interoperability Example: Monte Carlo pi

## Background

This directory contains a UPC++/Kokkos interoperability example 
for a Monte Carlo calculation of pi. The original material for both the source
file and the Makefile come from
[this exercise](https://github.com/kokkos/kokkos-tutorials/blob/main/Exercises/random_number/Solution) 
in the Kokkos-tutorials version 3.0 directory.

UPC++ concepts used by the example include reductions. Kokkos
concepts used by the example include Parallel Reductions and Random Number 
Generation. An explanation of Kokkos RNG concepts can be found in 
[this PowerPoint](https://github.com/kokkos/kokkos-tutorials/blob/main/Exercises/random_number/RNG-DemoExercises.pptx).

## Kokkos Configuration

The full set of Kokkos configuration options which can be utilized in a GNU 
Makefile can be found 
[here](https://github.com/kokkos/kokkos/wiki/Compiling#44-using-kokkos-gnu-makefile-system).
One required variable is `KOKKOS_PATH`, which must be set to the pathname 
containing Kokkos sources.
When targeting HIP devices it is also useful to specify the `KOKKOS_ARCH` 
variable, since without it GPU kernels might not be generated. A table showing 
the correct value for several device architectures (including CPUs) can be found
[here](https://kokkos.github.io/kokkos-core-wiki/keywords.html#architecture-keywords).
Only the part after the final underscore needs to be provided. For example, use 
`KOKKOS_ARCH=Vega908` to target AMD MI100 GPUs.

## UPC++ Configuration

This implementation depends on a working UPC++ v1.0 implementation (2020.11.0 or
later), with the upcxx compiler in the user's `PATH` or `UPCXX_INSTALL` set to the
installation directory. Regardless of whether the program's kernel runs on the
host or CUDA-capable devices, the UPC++ install need not be configured with CUDA support.
However, when this example is configured to run on a HIP-capable device the UPC++
version used must be at least 2022.3.3.

## Compiling and Running

This example can be compiled using the provided Makefile, and performance can
be optimized by use of the `CXXFLAGS`, `KOKKOS_DEVICES`, and `KOKKOS_ARCH` 
options. Your system must have the `KOKKOS_DEVICES` you specify (such as a 
CUDA-capable GPU), and compilation will fail if `KOKKOS_ARCH` doesn't match the
architecture of the CPU (and GPU, if any) you intend to run on.

There are additional runtime configuration options for other `KOKKOS_DEVICES`
to facilitate thread parallelism. For `Pthreads`, use the command line argument
`--kokkos-threads=n`. For `OpenMP`, set the environment variable `OMP_NUM_THREADS`.

Once compiled, run the program with this command:

```text
    upcxx-run -n <processes> [-N <nodes>] MC_DartSampler <args...>
```

The arguments are described in detail below.

### `KOKKOS_DEVICES=Pthreads` example: (4 processes, 8 threads/process)

```text
    upcxx-run -n 4 MC_DartSampler 23 --kokkos-threads=8
```

### `KOKKOS_DEVICES=OpenMP` example: (4 processes, 8 threads/process)

```text
    upcxx-run -n 4 env OMP_NUM_THREADS=8 MC_DartSampler 23
```

## Program Command Line Options

The command line arguments are:

* **required**: exponent for number of points (2^N) to randomly sample
    + must be <= 62 to avoid overflow
* optional: number of serial iterations for each kernel (not entire simulation) 
* optional: Any `--kokkos-*` arguments, as documented by Kokkos

