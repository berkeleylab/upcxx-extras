# Example Makefile for building the Monte Carlo pi calculation interoperability 
# example. System-specific modifications may be necessary, especially to 
# KOKKOS_PATH, which must be set to the Kokkos install directory or root of the 
# source tree.

ifeq ($(UPCXX_INSTALL),)
  $(warning UPCXX_INSTALL environment variable is not set, assuming upcxx is in the PATH)
  UPCXX=upcxx
else
  UPCXX=$(UPCXX_INSTALL)/bin/upcxx
endif

KOKKOS_PATH ?= 
ifeq ($(KOKKOS_PATH),)
  $(error KOKKOS_PATH is not set: should be path containing Kokkos source code)
endif
KOKKOS_DEVICES ?= Threads
KOKKOS_CUDA_OPTIONS ?= enable_lambda
KOKKOS_ARCH ?= 

# These lines convert device names to Title Case for compatibility with old versions of Kokkos
override KOKKOS_DEVICES := $(shell tr ',' ' ' <<<"$(KOKKOS_DEVICES)")
override KOKKOS_DEVICES := $(foreach device,$(KOKKOS_DEVICES),$(shell dev=$(device);$\
	echo $$(tr '[:lower:]' '[:upper:]' <<<$${dev:0:1})$$(tr '[:upper:]' '[:lower:]' <<<$${dev:1})))
override KOKKOS_DEVICES := $(shell tr ' ' ',' <<<"$(KOKKOS_DEVICES)")
override KOKKOS_DEVICES := $(subst Openmp,OpenMP,$(KOKKOS_DEVICES))

UPCXX_CODEMODE ?= opt
export UPCXX_CODEMODE

# Kokkos Makefiles parse the output of $CXX -version, 
# The following ensures it sees only the C++ compiler -version output it expects
UPCXX_VERSION_CLEAN = 1
export UPCXX_VERSION_CLEAN

EXE = MC_DartSampler

# Default build command/wrapper
CXX = $(UPCXX)
LINK = $(CXX)

# Override build command/wrapper when using a GPU device
ifneq (,$(findstring Cuda,$(KOKKOS_DEVICES)))
CXX = $(KOKKOS_PATH)/bin/nvcc_wrapper -ccbin $(UPCXX)
LINK = $(CXX)
endif
ifneq (,$(findstring Hip,$(KOKKOS_DEVICES)))
CXX = hipcc
LINK = $(UPCXX)
META_CPPFLAGS = $(shell $(UPCXX)-meta CPPFLAGS)
META_CXXFLAGS = $(shell $(UPCXX)-meta CXXFLAGS)
#META_LDFLAGS = $(shell $(UPCXX)-meta LDFLAGS)
#META_LIBS =    $(shell $(UPCXX)-meta LIBS)
endif

default: all

SRC = $(EXE:=.cpp)
OBJ = $(SRC:.cpp=.o)

include $(KOKKOS_PATH)/Makefile.kokkos

ifeq ($(LINK),$(UPCXX))
# filtering out hipcc-specific flags that might not be recognized by upcxx link step
KOKKOS_LDFLAGS := $(patsubst -fno-gpu-rdc,,$(KOKKOS_LDFLAGS))
KOKKOS_LDFLAGS := $(patsubst -fgpu-rdc,,$(KOKKOS_LDFLAGS))
KOKKOS_LDFLAGS := $(patsubst --amdgpu-target=%,,$(KOKKOS_LDFLAGS))
endif

# The two steps below are deliberately separate to avoid a parallel make race between 
# Kokkos file generation and the compilation commands that consume the generated code
all: force
	@$(MAKE) $(KOKKOS_CONFIG_HEADER)
	@$(MAKE) $(EXE)

$(EXE): $(OBJ) $(KOKKOS_LINK_DEPENDS)
	$(LINK) $(KOKKOS_LDFLAGS) $(META_LDFLAGS) $@.o $(KOKKOS_LIBS) $(META_LIBS) -o $@

clean: kokkos-clean 
	rm -f *.o $(EXE)

# Compilation rules

%.o:%.cpp $(KOKKOS_CPP_DEPENDS)
	$(CXX) $(KOKKOS_CPPFLAGS) $(META_CPPFLAGS) $(CXXFLAGS) $(META_CXXFLAGS) $(KOKKOS_CXXFLAGS) -c $< -o $@

.PHONY: force clean
