#include "config.hpp"
#include "utils.hpp"
#include <cuda.h>

using namespace upcxx::extras;

__global__ void GPU_jac3d(int Nx,int Ny, int Nz, double *d_U, double *d_Un,
        double *d_b, int pitch_floats, int pitch_bytes){
    double c = ((double) 1.0 / (double) 6.0);

    unsigned x = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned y = blockIdx.y * blockDim.y + threadIdx.y;
    unsigned z = blockIdx.z * blockDim.z + threadIdx.z;

    // Equivalent to: (1 <= x <= Nx && 1 <= y <= Ny && 1 <= z <= Nz)
    if ( (x-1) < Nx && (y-1) < Ny && (z-1) < Nz ) {
#ifdef INDEX_PITCHED_KERNEL
        double *ind = index_pitched(d_U, (z * (Ny + 2) + y), x, pitch_bytes);
        size_t ind_offset = ind - d_U;
        double *west = ind - 1;
        double *east = ind + 1;
        double *north = ind + pitch_floats;
        double *south = ind - pitch_floats;
        double *up = ind + ((Ny+2) * pitch_floats);
        double *down = ind - ((Ny+2) * pitch_floats);
#elif defined(BYTE_OFFSET_PITCHED_KERNEL)
        int ind = byte_offset_pitched<double>((z * (Ny + 2) + y), x,
                pitch_bytes);
        ind = ind / sizeof(*d_U);
        int west = ind - 1;
        int east = ind + 1;
        int north = ind + pitch_floats;
        int south = ind - pitch_floats;
        int up = ind + ((Ny+2) * pitch_floats);
        int down = ind - ((Ny+2) * pitch_floats);
#else
        int ind = (z * (Ny + 2) + y) * pitch_floats + x;
        int west = ind - 1;
        int east = ind + 1;
        int north = ind + pitch_floats;
        int south = ind - pitch_floats;
        int up = ind + ((Ny+2) * pitch_floats);
        int down = ind - ((Ny+2) * pitch_floats);
#endif

#ifdef INDEX_PITCHED_KERNEL
#ifdef POISSON
        d_Un[ind_offset] = (*west + *east + *south + *north+ *up + *down - d_b[ind_offset]) * c;
#else
        d_Un[ind_offset] = (*west + *east + *south + *north+ *up + *down) * c;
#endif

#else

#ifdef POISSON
        d_Un[ind] = (d_U[west] + d_U[east] + d_U[south] + d_U[north]+ d_U[up] + d_U[down] - d_b[ind]) * c;
#else
        d_Un[ind] = (d_U[west] + d_U[east] + d_U[south] + d_U[north]+ d_U[up] + d_U[down]) * c;
#endif
#endif
    }
}
