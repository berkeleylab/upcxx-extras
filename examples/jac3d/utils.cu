// Some useful utilities
// system includes
#include <stdio.h>
#include <iostream>
#include <assert.h>
#include <sstream>
#include <sys/time.h>
#include <cuda.h>
using namespace std;

extern int lastCudaErr(int rank, const char *mesg);

// External function definitions

// Report error
// Function style, example usage as follows:
//        cudaMalloc((void **) &a_d, size);
//        checkCUDAError("Unable to allocate storage on the device");

void OrderlyOutput(ostringstream& s1);

void checkCUDAError(const char *msg) {
    cudaError_t err = cudaGetLastError();
    if ( cudaSuccess != err)  {
        cerr << "Cuda error: " << msg << ": " << cudaGetErrorString( err) <<
            ".\n";
        exit(EXIT_FAILURE);
    }                         
}

extern __global__ void GPU_jac3d(int Nx,int Ny, int Nz,
        double *d_U,double *d_Un, double *d_B,
        int pitch_floats, int pitch_bytes);

void GPU_jac3d_driver(dim3& dimGrid, dim3& dimBlock, int nx, int ny,
        int nz, double *d_U, double *d_Un, double *d_B,
        size_t pitch_floats, size_t pitch_bytes) {
    GPU_jac3d<<<dimGrid, dimBlock>>>(nx, ny, nz, d_U, d_Un, d_B,
            pitch_floats, pitch_bytes);
}

// Accmulate and report last reported CUDA error value over all ranks

void  printMesh(int *n, double *U, const int rank){
    int nx = n[0] + 2;
    int ny = n[1] + 2;
    int nz = n[2] + 2;
    ostringstream sl;
    sl << "Rank: " << rank << ", N = [" << nx << ", " << ny << ", " << nz << "]" << endl;
    for (int k=0; k< nz; k++){
        sl << endl << "Plane " << k << ": " << endl;
        for (int j=0; j< ny; j++){
            for (int i=0; i< nx; i++){
                int ind = i + j*(nx) + k*(nx)*(ny);
                sl << U[ind] << " ";
            }
            sl << endl;
        }
    }
    OrderlyOutput(sl);
}

// Report device characteristics
// See https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__DEVICE.html#group__CUDART__DEVICE_1gb22e8256592b836df9a9cc36c9db7151 for interpretation of the various information that's available
int ReportDevice()
{
    int number_of_devices;
    cudaError_t  errCode = cudaGetDeviceCount(&number_of_devices);
    if ((errCode ==  cudaErrorNoDevice) || (errCode == cudaErrorInsufficientDriver)){
        cerr << "\n *** There are no available devices.\n";
        cerr << "     Either you are not attached to a compute node or\n";
        cerr << "     are not running in an appropraite batch queue.\n";
        cerr << "\n Exiting...\n\n";
        exit(EXIT_FAILURE);
    }
    cout << "# devices: " << number_of_devices << endl;;
    if (number_of_devices > 1) {
        cout << "\n%d Devices\n" << number_of_devices;
        int device_number;
        for (device_number = 0; device_number < number_of_devices;
                device_number++) {
            cudaDeviceProp deviceProp;
            assert(cudaSuccess == cudaGetDeviceProperties(&deviceProp, device_number));
            cout << "Device # " << device_number << ": " << deviceProp.name;
            cout << ".";
            cout << "Capability " <<  deviceProp.major << "." << deviceProp.minor << ",";
            cout  << deviceProp.multiProcessorCount << " cores" << endl;
            //                printf("Device # %d: %s. Capability %d.%d, %d cores\n",device_number,deviceProp.name, deviceProp.major, deviceProp.minor, deviceProp.multiProcessorCount);
        }
        cout << endl;
    }
    // get number of SMs on this GPU
    int devID;
    cudaGetDevice(&devID);
    cudaDeviceProp deviceProp;
    assert(cudaSuccess == cudaGetDeviceProperties(&deviceProp, devID));
    if (deviceProp.major == 9999 && deviceProp.minor == 9999) {
        cerr << "There is no device supporting CUDA.\n";
        exit(1);
    }
    // Output the characteristics
    // To report on others, see the following URL:
    // https://www.clear.rice.edu/comp422/resources/cuda/html/cuda-runtime-api/index.html#structcudaDeviceProp_1dee14230e417cb3059d697d6804da414

    cout << "Device is a " << deviceProp.name << ", ";
    cout << "capability: " <<  deviceProp.major << "." << deviceProp.minor << endl;

    cout << "Clock speed: " << ((double)deviceProp.clockRate)/1000 << "MHz\n" << endl;;
    cout << "# cores: " << deviceProp.multiProcessorCount << endl;
    double gb = 1024*1024*1024;
    cout << "\nGlobal memory: " << ((double)deviceProp.totalGlobalMem)/gb << " GB" << endl;
    cout << endl << "Memory Clock Rate (MHz): " <<  (double)deviceProp.memoryClockRate/1000 << endl;
    cout << "Memory Bus Width (bits): " <<  deviceProp.memoryBusWidth << endl;
    cout << "Peak Memory Bandwidth (GB/s): " <<  2.0*deviceProp.memoryClockRate*(deviceProp.memoryBusWidth/8)/1.0e6 << endl;
    cout << "L2 Cache size: (KB): " << (double)deviceProp.l2CacheSize/1024 << endl;
    if (deviceProp.ECCEnabled)
        cout << "ECC Enabled\n";
    else
        cout << "ECC NOT Enabled\n";

    if (deviceProp.asyncEngineCount == 1)
        cout << "Device can concurrently copy memory between host and device while executing a kernel\n";
    else if (deviceProp.asyncEngineCount == 2)
    {
        cout<< "Device can concurrently copy memory between host and device in both directions\n     and execute a kernel at the same time\n";
    }
    else if (deviceProp.asyncEngineCount == 0){
        cout << "Device CANNOT copy memory between host and device while executing a kernel.\n";
        cout << "Device CANNOT copy memory between host and device in both directions at the same time.\n";
    }
    if (deviceProp.unifiedAddressing == 1)
        cout << "Device shares a unified address space with the host\n";
    else
        cout << "Device DOES NOT share a unified address space with the host\n";
    cout<< "\n --------- \n";


    int driverVersion, runtimeVersion;
    assert(cudaSuccess == cudaDriverGetVersion(&driverVersion));
    assert(cudaSuccess == cudaRuntimeGetVersion(&runtimeVersion));
    cout << "CUDA Driver version: " <<  driverVersion << ", " << "runtime version: " << runtimeVersion << endl;

    return(100*deviceProp.major + deviceProp.minor);
}

double ***Alloc3D(int nx, int ny, int nz, char *mesg)
{
   double*** U = (double***)malloc(sizeof(double**)*nx + sizeof(double *)*nx*ny +sizeof(double)*nx*ny*nz);

   if (!U){
       fprintf(stderr,"Cannot allocate %s array\n" , mesg);
       cerr << "Cannot allocate " << mesg << " array\n";
       exit(-1);
   }
   for(int i=0;i<nx;i++){
       U[i] = ((double**) U) + nx + i*ny;
    }
   double *Ustart = (double *) (U[nx-1] + ny);
   for(int i=0;i<nx;i++)
       for(int j=0;j<ny;j++)
           U[i][j] = Ustart + i*ny*nz + j*nz;

  return U;
}
