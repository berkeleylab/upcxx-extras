#ifdef _GHOSTREGION_HPP
#else
#define _GHOSTREGION_HPP

#include <assert.h>
#include <upcxx/upcxx.hpp>

using namespace upcxx;
using namespace upcxx::extras;

static const unsigned int ND = 3;
static const unsigned X = 0, Y = 1, Z = 2;

template<typename V>
class ghostRegion{

private:
    upcxx::global_ptr<V, upcxx::memory_kind::cuda_device> remote_d_sndBuff[ND-1][2];
    upcxx::global_ptr<V, upcxx::memory_kind::cuda_device> remote_d_Un[2];
    upcxx::global_ptr<V, upcxx::memory_kind::cuda_device> remote_d_U[2];

    upcxx::global_ptr<V, upcxx::memory_kind::cuda_device> d_sndBuff[ND-1][2];
    upcxx::global_ptr<V, upcxx::memory_kind::cuda_device> d_rcvBuff[ND-1][2];
// The shape of the local mesh, with origin 0
    int d[ND];
    int p[ND];
    int _rank;
    int r[ND];
    int s[ND]; 	// Strides for neighbor rank offsets
    upcxx::future<> request_r[ND][2];
    dim3 dimGrid, dimBlock;
    int neighbors[ND][2];

    int g_nelt[2], g_len[2], scat_len[3];

    size_t required_dev_mem;
public:

    ~ghostRegion() {
        // Free Host and Device buffers
    }

    ghostRegion(int nk[ND], int pk[ND], int myrank_k[ND],
            dim3 _dimGrid, dim3 _dimBlock) {
        _rank = upcxx::rank_me();

        /*
         * s is the stride (measured in PEs) that it takes to get to a neighbor
         * in each dimension.
         *
         * pk is the number of PEs in each dimension.
         */
        s[X] = 1;
        s[Y] = pk[X];
        s[Z] = pk[X]*pk[Y];

        for (unsigned int i=0; i<ND; i++){
            d[i] = nk[i];
            p[i] = pk[i];
            r[i] = myrank_k[i]; // My offset in PEs in a given dimension
            // left/right, front/rear, down/up
            neighbors[i][0] =  (r[i] > 0       ) ? (_rank-s[i]) : -1;
            neighbors[i][1] =  (r[i] < (p[i]-1)) ? (_rank+s[i]) : -1;
        }
        dimGrid = _dimGrid;
        dimBlock = _dimBlock;

        g_nelt[0] = (d[Z]+2)*(d[Y]+2);
        g_nelt[1] = d[Z]+2;

        g_len[0] = 1;
        g_len[1] = d[X]+2;

        // Device memory required for serialization buffers
        required_dev_mem = sizeof(V) * ((d[Y]+2)*(d[Z]+2) + (d[X]+2)*(d[Z]+2) +
                (d[Y]+2)*(d[Z]+2) + (d[X]+2)*(d[Z]+2)) * 2;
    }

    size_t getRequiredDevMem() {
        return required_dev_mem;
    }

    void allocBuffs(upcxx::device_allocator<upcxx::cuda_device> &gpu_alloc,
            upcxx::global_ptr<V, upcxx::memory_kind::cuda_device> d_U,
            upcxx::global_ptr<V, upcxx::memory_kind::cuda_device> d_Un){
        for (int i = 0; i < 2; i++){
            // Device buffers
            d_sndBuff[X][i] = gpu_alloc.allocate<V>((d[Y]+2)*(d[Z]+2));
            assert(d_sndBuff[X][i]);
            d_sndBuff[Y][i] = gpu_alloc.allocate<V>((d[X]+2)*(d[Z]+2));
            assert(d_sndBuff[Y][i]);

            d_rcvBuff[X][i] = gpu_alloc.allocate<V>((d[Y]+2)*(d[Z]+2));
            assert(d_rcvBuff[X][i]);
            d_rcvBuff[Y][i] = gpu_alloc.allocate<V>((d[X]+2)*(d[Z]+2));
            assert(d_rcvBuff[Y][i]);
        }

        upcxx::dist_object<upcxx::global_ptr<V, upcxx::memory_kind::cuda_device>> dobj_sndBuffs[ND-1][2] = {
            {upcxx::dist_object<upcxx::global_ptr<V, upcxx::memory_kind::cuda_device>>(d_sndBuff[X][0]),
             upcxx::dist_object<upcxx::global_ptr<V, upcxx::memory_kind::cuda_device>>(d_sndBuff[X][1])},
            {upcxx::dist_object<upcxx::global_ptr<V, upcxx::memory_kind::cuda_device>>(d_sndBuff[Y][0]),
             upcxx::dist_object<upcxx::global_ptr<V, upcxx::memory_kind::cuda_device>>(d_sndBuff[Y][1])}
        };

        upcxx::dist_object<upcxx::global_ptr<V, upcxx::memory_kind::cuda_device>> dobj_d_U(d_U);
        upcxx::dist_object<upcxx::global_ptr<V, upcxx::memory_kind::cuda_device>>dobj_d_Un(d_Un);

        if (r[X] != 0){
            // Get the positive/right send buf of my left/negative X neighbor
            remote_d_sndBuff[X][0] = dobj_sndBuffs[X][1].fetch(neighbors[X][0]).wait();
        }

        if (r[X] != p[X]-1){
            // Get the negative/left send buf of my right/positive X neighbor
            remote_d_sndBuff[X][1] = dobj_sndBuffs[X][0].fetch(neighbors[X][1]).wait();
        }

        if (r[Y] != 0){
            // Get the positive/right send buf of my left/negative Y neighbor
            remote_d_sndBuff[Y][0] = dobj_sndBuffs[Y][1].fetch(neighbors[Y][0]).wait();
        }

        if (r[Y] != p[Y]-1){
            // Get the negative/left send buf of my right/positive Y neighbor
            remote_d_sndBuff[Y][1] = dobj_sndBuffs[Y][0].fetch(neighbors[Y][1]).wait();
        }

        if (r[Z] != 0){
            // Get the positive/right send buf of my left/negative Z neighbor
            remote_d_U[0] = dobj_d_U.fetch(neighbors[Z][0]).wait();
            remote_d_Un[0] = dobj_d_Un.fetch(neighbors[Z][0]).wait();
        }

        if (r[Z] != p[Z]-1){
            // Get the negative/left send buf of my right/positive Z neighbor
            remote_d_U[1] = dobj_d_U.fetch(neighbors[Z][1]).wait();
            remote_d_Un[1] = dobj_d_Un.fetch(neighbors[Z][1]).wait();
        }

        upcxx::barrier();
    }

    // Deallocate GPU buffers allocated from UPC++ CUDA allocator
    void deallocBuffs(upcxx::device_allocator<upcxx::cuda_device> &gpu_alloc) {
        for (int i=0; i<2; i++){
            gpu_alloc.deallocate(d_sndBuff[X][i]);
            gpu_alloc.deallocate(d_rcvBuff[X][i]);

            gpu_alloc.deallocate(d_sndBuff[Y][i]);
            gpu_alloc.deallocate(d_rcvBuff[Y][i]);
        }
    }

    /*
     * Gather results of this iteration from U into send buffers for fetching by
     * remote ranks.
     */
    void prepareDeviceBuffers(V *d_Un,
            upcxx::device_allocator<upcxx::cuda_device> &gpu_alloc,
            size_t pitch_bytes, size_t pitch_floats) {
        if(r[X] != 0){
            gather_GPU(gpu_alloc.local(d_sndBuff[X][0]),
                    index_pitched(d_Un, 0, 1, (int)pitch_bytes),
                    g_nelt[0], g_len[0], pitch_floats);
        }

        if(r[X] != p[X]-1){
            gather_GPU(gpu_alloc.local(d_sndBuff[X][1]),
                    index_pitched(d_Un, 0, d[X], (int)pitch_bytes),
                    g_nelt[0], g_len[0], pitch_floats);
        }

        if(r[Y]!=0){
            gather_GPU(gpu_alloc.local(d_sndBuff[Y][0]),
                    index_pitched(d_Un, 1, 0, (int)pitch_bytes),
                    g_nelt[1], g_len[1], (pitch_floats)*(d[Y]+2));
        }

        if(r[Y]!=p[Y]-1){
            gather_GPU(gpu_alloc.local(d_sndBuff[Y][1]),
                    index_pitched(d_Un, d[Y], 0, (int)pitch_bytes),
                    g_nelt[1], g_len[1], (pitch_floats)*(d[Y]+2));
        }

        gpuErrchk(cudaDeviceSynchronize()); 
        gpuErrchk(cudaGetLastError());
    }

    void fillGPU(upcxx::global_ptr<V, upcxx::memory_kind::cuda_device> d_Un,
            upcxx::device_allocator<upcxx::cuda_device> &gpu_alloc,
            size_t pitch_bytes, size_t pitch_floats) {
        V* local_d_Un = gpu_alloc.local(d_Un);

        prepareDeviceBuffers(local_d_Un, gpu_alloc, pitch_bytes, pitch_floats);

        /*
         * Ensure everyone has finished populating device buffers before we
         * start copying.
         */
        upcxx::barrier();

        /*
         * Fetch 3D ghost region into my local buffers, copying directly from
         * remote GPUs to my local GPU using UPC++ memory kinds support. Then,
         * use futures to deserialize the fetched data into my local U.
         */
        upcxx::future<> fut = upcxx::make_future();
        if(r[X] != 0){
            fut = upcxx::when_all(fut, upcxx::copy(remote_d_sndBuff[X][0],
                        d_rcvBuff[X][0], (d[Z]+2)*(d[Y]+2)).then([&] {
                            scatter_GPU(
                                index_pitched(local_d_Un, 0, 0, (int)pitch_bytes),
                                gpu_alloc.local(d_rcvBuff[X][0]), g_nelt[0], 1,
                                pitch_floats);
                        }));
        }

        if(r[X] != p[X]-1){
            fut = upcxx::when_all(fut, upcxx::copy(remote_d_sndBuff[X][1],
                        d_rcvBuff[X][1], (d[Z]+2)*(d[Y]+2)).then([&] {
                            scatter_GPU(
                                index_pitched(local_d_Un, 0, d[X]+1,
                                    (int)pitch_bytes),
                                gpu_alloc.local(d_rcvBuff[X][1]), g_nelt[0], 1,
                                pitch_floats);
                        }));
        }

        if(r[Y]!=0){
            fut = upcxx::when_all(fut, upcxx::copy(remote_d_sndBuff[Y][0],
                        d_rcvBuff[Y][0], (d[Z]+2)*(d[X]+2)).then([&] {
                            scatter_GPU(
                                index_pitched(local_d_Un, 0, 0, (int)pitch_bytes),
                                gpu_alloc.local(d_rcvBuff[Y][0]), d[Z]+2,
                                d[X]+2, (d[Y]+2)*(pitch_floats));
                        }));
        }

        if(r[Y]!=p[Y]-1){
            fut = upcxx::when_all(fut, upcxx::copy(remote_d_sndBuff[Y][1],
                        d_rcvBuff[Y][1], (d[Z]+2)*(d[X]+2)).then([&] {
                            scatter_GPU(
                                index_pitched(local_d_Un, d[Y]+1, 0,
                                    (int)pitch_bytes),
                                gpu_alloc.local(d_rcvBuff[Y][1]), d[Z]+2,
                                d[X]+2, (d[Y]+2)*pitch_floats);
                        }));
        }

        if(r[Z]!=0){
            fut = upcxx::when_all(fut,
                    upcxx::copy(
                        index_pitched(remote_d_Un[0], d[Z]*(d[Y]+2), 0,
                            (int)pitch_bytes),
                        d_Un,
                        pitch_floats*(d[Y]+2)));
        }

        if(r[Z]!=p[Z]-1){
            fut = upcxx::when_all(fut,
                    upcxx::copy(
                        index_pitched(remote_d_Un[1], (d[Y]+2), 0, (int)pitch_bytes),
                        index_pitched(d_Un, (d[Z]+1)*(d[Y]+2), 0, (int)pitch_bytes),
                        pitch_floats*(d[Y]+2)));
        }

        // Wait for all my upcxx::copy to complete
        fut.wait();

        // Ensure the scatter kernels have completed locally
        gpuErrchk(cudaDeviceSynchronize());

        std::swap(remote_d_Un, remote_d_U);

        // Ensure everyone has finished before we start computing
        upcxx::barrier();
    }
};
#endif
