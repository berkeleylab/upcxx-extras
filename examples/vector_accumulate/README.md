This directory contains an example of using UPC++ RPC and Views to perform
remote vector accumulates. It includes an example for both trivially and
non-trivially serializable types.

This implementation depends on a working UPC++ v1.0 implementation (2020.3.0 or
later), with the upcxx compiler in the user's PATH or UPCXX_INSTALL set to the
installation directory.

This example can be compiled using the provided Makefile, though some
system-specific changes may be necessary:

    make
