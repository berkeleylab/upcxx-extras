#include "initMatrix.hpp"

__global__ void initMatrixKernel(double *A, double *B, int M, double Ainit,
        double Binit) {
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    if (tid < M) {
        A[tid] = Ainit;
        B[tid] = Binit;
    }
}

void initMatrixOnDevice(double *A, double *B, int M, double Ainit, double Binit) {
    const int threadsPerBlock = 512;
    const int blocksPerGrid = (M + threadsPerBlock - 1) / threadsPerBlock;
    initMatrixKernel<<<blocksPerGrid, threadsPerBlock>>>(A, B, M, Ainit, Binit);
}
