# 2D Heat Diffusion

This directory contains a minimal example that simulates heat diffusion in two
dimensions by using UPC++ distributed objects and asynchronous RMA puts. Domain
decomposition across the processes is one-dimensional for simplicity.

* This example uses only the host CPU for computation. 
* Each process exposes its two boundaries (one for highest and lowest rank) via `dist_object`
* _Futures_  and _barriers_ are used to synchronize communication of halo cells across processes

## UPC++ Configuration

This example depends on a working UPC++ v1.0 library installation, with the
`upcxx` compiler wrapper in the user's `$PATH` or `$UPCXX_INSTALL` set to the
installation directory.

## Compiling and Running

This example can be compiled using the provided Makefile:

```sh
    $ make
```

One can optionally modify build-time parameters such as the UPC++ network
backend as follows:

```sh
    $ make UPCXX_NETWORK=smp
```

Make builds an executable named `heat2d`.  Within an appropriate environment
capable of executing UPC++ jobs, this can be run with a command of the form:

```sh
    $ upcxx-run -n <processes> [-N <nodes>] ./heat2d [args...]
```
Where `<processes>` is the number of processes to use (degree of parallelism)
and `<nodes>` is optionally the number of physical compute nodes to use.
`args...` may include zero or more of the following
positional command line arguments (all integers):

* `X`: Width of the computational domain (number of points, defaults to 4096)
* `Y`: Height of the computational domain (number of points, defaults to X)
* `N`: Number of time-steps (defaults to 50)

So for example on a distributed system, the following command:

```sh
    $ upcxx-run -n 8 -N 2 ./heat2d 8192
```

would run a 8192^2 problem size on 8 processes distributed across 2 compute nodes.

---

For a more fully featured heat diffusion example, see the [kokkos_3dhalo example](../kokkos_3dhalo),
which is 3D and optionally offloads computation to a GPU accelerator.


