/* Simulates 2D heat diffusion with fixed boundaries and 1D domain decomposition.
   Ranks allocate buffers at interior boundaries for halo communication. 
 */ 
#include<algorithm>
#include<upcxx/upcxx.hpp>

using upcxx::experimental::say;

int X=4096, Y=X; // domain size
int N=50; // number of timesteps
double alpha=0.25; // thermal transfer coefficient
double T0=1.0; // initial temperature

int lo, hi; // local slab boundary in Y coordinates

double *T_old, *T_new; // Temperature and delta Temperature
double *T_up, *T_down; // Halo data
upcxx::global_ptr<double> gptr_up, gptr_down; // Destination for peer's halo data

upcxx::future<> fut_all; // tracks asynchronous communication completion

void setup_subdomain() {
  // compute domain decomposition
  upcxx::intrank_t my_rank = upcxx::rank_me(), num_ranks = upcxx::rank_n();
  int dY = Y/num_ranks;
  if (my_rank < Y%num_ranks) dY++;
  lo = dY*my_rank;
  if (my_rank >= Y%num_ranks) lo += Y%num_ranks;
  hi = lo + dY;
  // compute neighbor process ranks
  upcxx::intrank_t down = (my_rank == 0           ? -1 : my_rank-1);
  upcxx::intrank_t up =   (my_rank == num_ranks-1 ? -1 : my_rank+1);
  say() << "\tMy Neighbors: (" << down << ", " << up << ")     "
        << "\tMy Domain: (" << lo << ',' << hi << ')';
  UPCXX_ASSERT_ALWAYS(dY > 1, "This code only supports up to Y/2=" << Y/2 << " ranks");

  T_old = new double[X * (hi-lo)];
  T_new = new double[X * (hi-lo)];
  std::fill_n(T_old,X*(hi-lo),T0);
  std::fill_n(T_new,X*(hi-lo),T0);

  for (int i=std::max(Y/4,lo); i < std::min(Y/2+1,hi); i++) 
    std::fill_n(T_old+(i-lo)*X+X/4,(X+1)/2-X/4,2*T0); // sets tile at intersection of X and Y's 2nd quartile

  upcxx::global_ptr<double> down_in, up_in;
  if(lo != 0) {
      down_in = upcxx::new_array<double>(X);
      T_down = down_in.local();
  }
  if(hi != Y) {
      up_in = upcxx::new_array<double>(X);
      T_up = up_in.local();
  }
  upcxx::dist_object<upcxx::global_ptr<double>> dist_up{down_in}; 
  upcxx::dist_object<upcxx::global_ptr<double>> dist_down{up_in}; 
  // get the addresses of neighboring ranks' halo buffer
  if(lo != 0) gptr_down = dist_down.fetch(down).wait();
  if(hi != Y) gptr_up = dist_up.fetch(up).wait();
  upcxx::barrier();
}

void exchange_T_halo() {
  // Start asynchronous rput of boundary data into neighboring ranks' halo buffers.
  // rput returns a upcxx::future handle that is conjoined for later completion step
  fut_all = upcxx::make_future();
  if(lo != 0) fut_all = upcxx::when_all(fut_all, 
                          upcxx::rput(T_old,gptr_down,X) );
  if(hi != Y) fut_all = upcxx::when_all(fut_all, 
                          upcxx::rput(&T_old[X*(hi-lo-1)],gptr_up,X) );
}

void compute_inner_T_new() { // compute update for inner grid points
  // T_new(x,y) = T_old(x,y) + alpha * sum_dxdy( T_old(x+dx,y+dy) - T_old(x,y) )
  for (int j=1; j<(hi-lo-1); j++) 
    for (int i=1; i<X-1; i++) {
      size_t idx = i + X*j;
      T_new[idx] = T_old[idx] + alpha * (T_old[idx-1] + T_old[idx-X] - 4*T_old[idx] + T_old[idx+1] + T_old[idx+X]);
    }
}

void compute_surface_T_new() { // compute update for surface grid points
  fut_all.wait(); // Wait for rputs initiated by this rank to complete 
  // Once all ranks reach barrier below, they've all completed putting their data, 
  // which implies that after the barrier incoming halo data has arrived at all ranks
  upcxx::barrier();
  if (lo != 0)
    for (int i=1; i<X-1; i++)
      T_new[i] = T_old[i] + alpha * (T_down[i] + T_old[i-1] - 4*T_old[i] + T_old[i+1] + T_old[i+X]); 
  if (hi != Y)
    for (int i=1; i<X-1; i++) {
      size_t idx = i+X*(hi-lo-1); 
      T_new[idx] = T_old[idx] + alpha * (T_old[idx-X] + T_old[idx-1] - 4*T_old[idx] + T_old[idx+1] + T_up[i]); 
    }
}

void timestep() { // run the time-stepping loop
  auto begin = std::chrono::steady_clock::now();
  for(int t=0; t<N; t++) {
    exchange_T_halo();
    compute_inner_T_new();
    compute_surface_T_new();
    std::swap(T_new,T_old);
    upcxx::barrier();
  }
  double local_sum = std::accumulate(T_old,T_old+X*(hi-lo),0.);  // compute temperature sum on this rank
  // reduction of all local_sums on rank 0
  double global_sum = upcxx::reduce_one(local_sum,upcxx::op_fast_add,0).wait();
  if(upcxx::rank_me() == 0) {
    double T_ave = global_sum/(X*Y);
    auto time = std::chrono::duration<double>(std::chrono::steady_clock::now() - begin).count();
    say() << "mean temperature=" << T_ave << " | Solve time: " << time << " seconds";
  }
}

int main(int argc, char* argv[]) {
  upcxx::init();
  if (argc>1) X = Y = atoi(argv[1]);
  if (argc>2) Y = atoi(argv[2]);
  if (argc>3) N = atoi(argv[3]);
  setup_subdomain();
  timestep();
  upcxx::finalize();
}
