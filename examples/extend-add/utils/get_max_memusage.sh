#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Illegal number of parameters.\n$0 FILENAME"
  exit
fi
if [ ! -f $1 ]; then
  echo "$1 is not a valid filename"
  exit
fi

filename=$1
proc=$(basename $filename .dmp | sed -n 's/.*_\([0-9]\+\)$/\1/p') 
max_upd_nnz_per_proc=$(awk '{print $4, $9}' $filename | awk 'FNR == 1 {next} {$3 = ($2*$2) / $1}1' | awk '{print $3}' | sort -n | tail -n 1)
mb_size=$( echo "$max_upd_nnz_per_proc*8/(1024*1024)" | bc)
dbl_mb_size=$(echo "($mb_size*1.5)/1" | bc)
if [ $proc -gt 1 ]
then
  echo "Suggested option: -shared-heap ${dbl_mb_size}MB"
else
  echo "No additional option suggested"
fi
