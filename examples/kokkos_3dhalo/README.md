# UPC++/Kokkos Interoperability Example: 3D Heat Exchange

## Background

This directory contains an interoperability example of using UPC++ with Kokkos
to simulate heat conduction in a 3D object. The original material for both the
source file and the Makefile come from 
[this exercise](https://github.com/kokkos/kokkos-tutorials/tree/main/Exercises/mpi_heat_conduction/Solution)
in the Kokkos-tutorials version 3.0 directory and uses MPI message-passing
between ranks instead of UPC++.

UPC++ concepts used by the example include `dist_object`, `promise` and `future`. 
Kokkos concepts used by the example include Execution Spaces, Views, MDRangePolicies,
Parallel Loops, and Parallel Reductions. For an explanation of these Kokkos
concepts, as well as a discussion of the simulation's optimal algorithm for
overlapping computation with data transfer, look at slides 42-45 of 
[this Kokkos tutorial](https://github.com/kokkos/kokkos-tutorials/blob/main/LectureSeries/KokkosTutorial_06_FortranPythonMPIAndPGAS.pdf).

## Kokkos Configuration

The full set of Kokkos configuration options which can be utilized in a GNU 
Makefile can be found 
[here](https://github.com/kokkos/kokkos/wiki/Compiling#44-using-kokkos-gnu-makefile-system).
One required variable is `KOKKOS_PATH`, which must be set to the pathname 
containing Kokkos sources.
When targeting HIP devices it is also useful to specify the `KOKKOS_ARCH` 
variable, since without it GPU kernels might not be generated. A table showing 
the correct value for several device architectures (including CPUs) can be found
[here](https://kokkos.github.io/kokkos-core-wiki/keywords.html#architecture-keywords).
Only the part after the final underscore needs to be provided. For example, use 
`KOKKOS_ARCH=Vega908` to target AMD MI100 GPUs.

## UPC++ Configuration

The host implementation depends on a working UPC++ v1.0 implementation (2020.11.0 or
later), with the upcxx compiler in the user's `PATH` or `UPCXX_INSTALL` set to the
installation directory. The GPU variant depends on a UPC++ implementation configured
with support for a device kind. CUDA support requires UPC++ version 2021.9.5 or later, 
while HIP support requires UPC++ version 2022.3.3 or later. 

## Compiling and Running

This example can be compiled using the provided Makefile, and performance can
be optimized by use of the `CXXFLAGS`, `KOKKOS_DEVICES`, and `KOKKOS_ARCH` 
options. Your system must have the `KOKKOS_DEVICES` you specify (such as a 
CUDA-capable GPU), and compilation will fail if `KOKKOS_ARCH` doesn't match the
architecture of the CPU (and GPU, if any) you intend to run on.

Furthermore, the executable produced by the `Makefile` will differ based on the 
`KOKKOS_DEVICES` specified. If a `Cuda` build is requested, then the resulting 
binary will be `upcxx_heat_conduction`, which exchanges data buffers through 
device memory. If a different device model is used, then the Makefile will 
produce `host_upcxx_heat_conduction`, in which the data buffers that are 
exchanged between ranks reside on the host.

There are additional runtime configuration options for certain `KOKKOS_DEVICES`
to facilitate thread parallelism. For `Pthreads`, use the command line argument
`--kokkos-threads=n`. For `OpenMP`, set the environment variable 
`OMP_NUM_THREADS`.

Once compiled, run the program with this command, remembering that the `host_` 
prefix to the executable name should be used if `Cuda` was not listed in 
`KOKKOS_DEVICES`:

```text
    upcxx-run -n <processes> [-N <nodes>] [host_]upcxx_heat_conduction <args...>
```

The arguments are described below.

## Program Command Line Options

```text
format: -flag description (default)

-X num elements in X direction (100)
-Y num elements in Y direction (100)
-Z num elements in Z direction (100)
-N num timesteps (100)
-I print interval (N)
-T0 initial temperature (0.0)
-dt timestep size (0.1)
-q thermal conductivity (1.0)
-sigma thermal radiation (1.0)
-P incoming power (1.0)
```

Note that the domain decomposition cannot be customized.

## Performance

The performance of this example code is documented in the following publication,
which demonstrates good scaling up to 3,072 processes on the OLCF Summit supercomputer:

 Daniel Waters, Colin A. MacLean, Dan Bonachea, Paul H. Hargrove.  
"**Demonstrating UPC++/Kokkos Interoperability in a Heat Conduction Simulation**",  
In *[2021 IEEE/ACM Parallel Applications Workshop, Alternatives To MPI+X (PAW-ATM'21)](https://go.lbl.gov/paw-atm21)* St. Louis, MO, Nov 2021. 5 pages.    
Paper: <https://doi.org/10.25344/S4630V>      
[Video presentation](https://doi.org/10.25344/S4DK5F)    
[Talk slides](https://upcxx.lbl.gov/wiki/pubs/PAW-ATM21-upcxx-kokkos_slides.pdf)

