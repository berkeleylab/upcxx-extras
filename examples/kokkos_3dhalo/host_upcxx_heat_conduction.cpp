/* This example has been adapted from a Kokkos/MPI interoperability example in 
 * the Kokkos Tutorials repository (https://github.com/kokkos/kokkos-tutorials/)
 * as allowed under terms specified in the LICENSE.txt file that can be found in
 * this directory.
 *
 * This variant of the test supports any Kokkos device, and always communicates
 * the Halo exchange through buffers staged in host memory.
*/

#include<Kokkos_Core.hpp>
#include<upcxx/upcxx.hpp>

#ifndef USE_HALO_BARRIER
#define USE_HALO_BARRIER 1 // is barrier used to synchronize halo exchange
#endif

typedef struct _Expected {
    int length, iters;
    double avg;
} Expected;

static Expected mpi_avg_temp[] = {
    {20,10,     28910001.659627},
    {20,100,   101705231.273039},
    {20,1000,  226372573.760934},
    {20,10000, 132848459.167244},
    {100,10,     5808348.947955},
    {100,100,   21452388.032534},
    {100,1000,  58583907.520696},
    {100,10000,135304050.230331},
    {200,10,     2905829.475261},
    {200,100,   10799013.696056},
    {200,1000,  30229622.441892},
    {200,10000, 77422551.027710}
};

bool valid = true;

static void validate(int size, int iters, double avg) {
    if (valid) {
        for (size_t i = 0; i < sizeof(mpi_avg_temp)/sizeof(mpi_avg_temp[0]); i++) {
            Expected *mpi = &mpi_avg_temp[i];
            if (mpi->length == size && mpi->iters == iters) {
                double abs = std::fabs(avg - mpi->avg);
                double rel = abs/mpi->avg;
                printf("Average temperature: expected = %e, calculated = %e, "
                        "absolute error = %e, percent error = %e\n",
                        mpi->avg, avg, abs, rel);
                if (rel > 0.01 || std::isnan(rel))
                    printf("FAIL\n");
                else
                    printf("SUCCESS\n");
                return;
            }
        }
    }
    printf("WARNING: Unable to validate simulation, unsupported input arguments\n");
    printf("SUCCESS\n");
}

template <class ExecSpace>
struct SpaceInstance {
  static ExecSpace create() { return ExecSpace(); }
  static void destroy(ExecSpace&) {}
};

struct CommHelper {
  upcxx::team &team;

  // Num UPC++ ranks in each dimension
  int nx, ny, nz;

  // My rank
  upcxx::intrank_t me;

  // My pos in proc grid
  int x, y, z;

  // Neighbor Ranks
  int up,down,left,right,front,back;
 
  CommHelper(upcxx::team &team_) : team(team_) {
    upcxx::intrank_t nranks = team.rank_n();
    me = team.rank_me();

    nx = std::pow(1.0*nranks,1.0/3.0);
    while(nranks%nx != 0) nx++;

    ny = std::sqrt(1.0*(nranks/nx));
    while((nranks/nx)%ny != 0) ny++;
    
    nz = nranks/nx/ny;
    x = me%nx;
    y = (me/nx)%ny;
    z = (me/nx/ny);
    left  = x==0 ?-1:me-1;
    right = x==nx-1?-1:me+1;
    down  = y==0 ?-1:me-nx;
    up    = y==ny-1?-1:me+nx;
    front = z==0 ?-1:me-nx*ny;
    back  = z==nz-1?-1:me+nx*ny;

#if !UPCXX_CODEMODE // Debug only:
    printf("NumRanks: %i Me: %i Grid: %i %i %i MyPos: %i %i %i\n",nranks,me,nx,ny,nz,x,y,z);
    printf("Me: %i MyNeighs: %i %i %i %i %i %i\n",me,left,right,down,up,front,back);
#endif
    upcxx::barrier(team);
  }
};

#if !USE_HALO_BARRIER
int count = 0;
#endif

struct System {
  // Using theoretical physicists way of describing system, 
  // i.e. we stick everything in as few constants as possible
  // be i and i+1 two timesteps dt apart: 
  // T(x,y,z)_(i+1) = T(x,y,z)_(i)+dT(x,y,z)*dt; 
  // dT(x,y,z) = q * sum_dxdydz( T(x+dx,y+dy,z+dz) - T(x,y,z) )
  // If its surface of the body add:
  // dT(x,y,z) += -sigma*T(x,y,z)^4
  // If its z==0 surface add incoming radiation energy
  // dT(x,y,0) += P

  // Communicator
  CommHelper comm;
#if USE_HALO_BARRIER
  upcxx::future<> fut_all;
#endif
  int nbors;

  // size of system
  int X,Y,Z;

  // Local box
  int X_lo, Y_lo, Z_lo;
  int X_hi, Y_hi, Z_hi;

  // number of timesteps
  int N;
  
  // interval for print
  int I;

  // Temperature and delta Temperature
  Kokkos::View<double***> T, dT;
  // Halo data
  using buffer_t = Kokkos::View<double**,Kokkos::LayoutRight,Kokkos::HostSpace>;
  buffer_t T_left, T_right, T_up, T_down, T_front, T_back;
  buffer_t T_left_out, T_right_out, T_up_out, T_down_out, T_front_out, T_back_out;

  Kokkos::DefaultExecutionSpace E_left, E_right, E_up, E_down, E_front, E_back, E_bulk;

  upcxx::global_ptr<double> gptr_left, gptr_right, gptr_up, gptr_down, gptr_front, gptr_back;

  // Initial temperature
  double T0;

  // timestep width
  double dt;

  // thermal transfer coefficient 
  double q;

  // thermal radiation coefficient (assume Stefan Boltzmann law P = sigma*A*T^4
  double sigma;

  // incoming power
  double P;

  // init_system
  
  System(upcxx::team &_comm) : comm(_comm) {
#if USE_HALO_BARRIER
    fut_all = upcxx::make_future<>();
#endif
    nbors = 0;
    X = Y = Z = 100;
    X_lo = Y_lo = Z_lo = 0;
    X_hi = Y_hi = Z_hi = X; 
    N = 100;
    I = N;
    T = Kokkos::View<double***>();
    dT = Kokkos::View<double***>();
    T0 = 0.0;
    dt = 0.1;
    q = 1.0;
    sigma = 1.0;
    P = 1.0;
    E_left  = SpaceInstance<Kokkos::DefaultExecutionSpace>::create();
    E_right = SpaceInstance<Kokkos::DefaultExecutionSpace>::create();
    E_up    = SpaceInstance<Kokkos::DefaultExecutionSpace>::create();
    E_down  = SpaceInstance<Kokkos::DefaultExecutionSpace>::create();
    E_front = SpaceInstance<Kokkos::DefaultExecutionSpace>::create();
    E_back  = SpaceInstance<Kokkos::DefaultExecutionSpace>::create();
    E_bulk  = SpaceInstance<Kokkos::DefaultExecutionSpace>::create();
  }

  void destroy_exec_spaces() {
    SpaceInstance<Kokkos::DefaultExecutionSpace>::destroy(E_left);
    SpaceInstance<Kokkos::DefaultExecutionSpace>::destroy(E_right);
    SpaceInstance<Kokkos::DefaultExecutionSpace>::destroy(E_front);
    SpaceInstance<Kokkos::DefaultExecutionSpace>::destroy(E_back);
    SpaceInstance<Kokkos::DefaultExecutionSpace>::destroy(E_up);
    SpaceInstance<Kokkos::DefaultExecutionSpace>::destroy(E_down);
    SpaceInstance<Kokkos::DefaultExecutionSpace>::destroy(E_bulk);
  }

  void setup_subdomain() {
    int dX = (X+comm.nx-1)/comm.nx;
    X_lo = dX*comm.x;
    X_hi = X_lo + dX;
    if(X_hi>X) X_hi=X;
    int dY = (Y+comm.ny-1)/comm.ny;
    Y_lo = dY*comm.y;
    Y_hi = Y_lo + dY;
    if(Y_hi>Y) Y_hi=Y;
    int dZ = (Z+comm.nz-1)/comm.nz;
    Z_lo = dZ*comm.z;
    Z_hi = Z_lo + dZ;
    if(Z_hi>Z) Z_hi=Z;

#if !UPCXX_CODEMODE // Debug only:
    printf("My Domain: %i (%i %i %i) (%i %i %i)\n",comm.me,X_lo,Y_lo,Z_lo,X_hi,Y_hi,Z_hi);
#endif
    T = Kokkos::View<double***>("System::T", X_hi - X_lo, Y_hi - Y_lo, Z_hi - Z_lo);
    dT = Kokkos::View<double***>("System::dT", T.extent(0), T.extent(1), T.extent(2));
    Kokkos::deep_copy(T,T0);

    upcxx::global_ptr<double> left_in, right_in, down_in, up_in, front_in, back_in;
    if(X_lo != 0) {
        left_in = upcxx::new_array<double>(T.extent(1)*T.extent(2));
        T_left  = buffer_t(left_in.local(), T.extent(1), T.extent(2));
        T_left_out  = buffer_t("T_left_out", T.extent(1), T.extent(2));
        nbors++;
    }
    if(X_hi != X) {
        right_in = upcxx::new_array<double>(T.extent(1)*T.extent(2));
        T_right = buffer_t(right_in.local(), T.extent(1), T.extent(2));
        T_right_out = buffer_t("T_right_out", T.extent(1), T.extent(2));
        nbors++;
    }
    if(Y_lo != 0) {
        down_in = upcxx::new_array<double>(T.extent(0)*T.extent(2));
        T_down  = buffer_t(down_in.local(), T.extent(0), T.extent(2));
        T_down_out  = buffer_t("T_down_out", T.extent(0), T.extent(2));
        nbors++;
    }
    if(Y_hi != Y) {
        up_in = upcxx::new_array<double>(T.extent(0)*T.extent(2));
        T_up    = buffer_t(up_in.local(), T.extent(0), T.extent(2));
        T_up_out = buffer_t("T_up_out", T.extent(0), T.extent(2));
        nbors++;
    }
    if(Z_lo != 0) {
        front_in = upcxx::new_array<double>(T.extent(0)*T.extent(1));
        T_front = buffer_t(front_in.local(), T.extent(0), T.extent(1));
        T_front_out = buffer_t("T_front_out", T.extent(0), T.extent(1));
        nbors++;
    }
    if(Z_hi != Z) {
        back_in = upcxx::new_array<double>(T.extent(0)*T.extent(1));
        T_back  = buffer_t(back_in.local(), T.extent(0), T.extent(1));
        T_back_out  = buffer_t("T_back_out", T.extent(0), T.extent(1));
        nbors++;
    }
    upcxx::dist_object<upcxx::global_ptr<double>> dist_left{right_in, comm.team}; 
    upcxx::dist_object<upcxx::global_ptr<double>> dist_right{left_in, comm.team}; 
    upcxx::dist_object<upcxx::global_ptr<double>> dist_up{down_in, comm.team}; 
    upcxx::dist_object<upcxx::global_ptr<double>> dist_down{up_in, comm.team}; 
    upcxx::dist_object<upcxx::global_ptr<double>> dist_back{front_in, comm.team}; 
    upcxx::dist_object<upcxx::global_ptr<double>> dist_front{back_in, comm.team}; 
    if(X_lo != 0)
        gptr_left = dist_left.fetch(comm.left).wait();
    if(X_hi != X)
        gptr_right = dist_right.fetch(comm.right).wait();
    if(Y_lo != 0)
        gptr_down = dist_down.fetch(comm.down).wait();
    if(Y_hi != Y)
        gptr_up = dist_up.fetch(comm.up).wait();
    if(Z_lo != 0)
        gptr_front = dist_front.fetch(comm.front).wait();
    if(Z_hi != Z)
        gptr_back = dist_back.fetch(comm.back).wait();
    upcxx::barrier(comm.team);
  }

  void print_help() {
    printf("Options (default):\n");
    printf("  -X IARG: (%i) num elements in X direction\n", X); 
    printf("  -Y IARG: (%i) num elements in Y direction\n", Y); 
    printf("  -Z IARG: (%i) num elements in Z direction\n", Z); 
    printf("  -N IARG: (%i) num timesteps\n", N); 
    printf("  -I IARG: (%i) print interval\n", I); 
    printf("  -T0 FARG: (%lf) initial temperature\n", T0); 
    printf("  -dt FARG: (%lf) timestep size\n", dt); 
    printf("  -q FARG: (%lf) thermal conductivity\n", q); 
    printf("  -sigma FARG: (%lf) thermal radiation\n", sigma); 
    printf("  -P FARG: (%lf) incoming power\n", P);
  }

  // check command line args
  bool check_args(int argc, char* argv[]) {
    for(int i=1; i<argc; i++) {
      if(strcmp(argv[i],"-h")==0) { print_help(); return false; }
    }
    for(int i=1; i<argc; i++) {
      if(strcmp(argv[i],"-X")==0) X = atoi(argv[i+1]);
      if(strcmp(argv[i],"-Y")==0) Y = atoi(argv[i+1]);
      if(strcmp(argv[i],"-Z")==0) Z = atoi(argv[i+1]);
      if(strcmp(argv[i],"-N")==0) N = atoi(argv[i+1]);
      if(strcmp(argv[i],"-I")==0) I = atoi(argv[i+1]);
      if(strcmp(argv[i],"-T0")==0) {
          T0 = atof(argv[i+1]);
          valid = false;
      }
      if(strcmp(argv[i],"-dt")==0) {
          dt = atof(argv[i+1]);
          valid = false;
      }
      if(strcmp(argv[i],"-q")==0) {
          q = atof(argv[i+1]);
          valid = false;
      }
      if(strcmp(argv[i],"-sigma")==0) {
          sigma = atof(argv[i+1]);
          valid = false;
      }
      if(strcmp(argv[i],"-P")==0) {
          P = atof(argv[i+1]);
          valid = false;
      }
    }
    if (X != Y || X != Z) valid = false;
    setup_subdomain();
    return true;
  }

  // run_time_loops
  void timestep() {
    Kokkos::Timer timer;
    double old_time = 0.0;
    for(int t=0; t<=N; t++) {
      if(t>N/2) P = 0.0;
      pack_T_halo();
      compute_inner_dT();
      exchange_T_halo();
      compute_surface_dT();
      Kokkos::fence();
      double T_ave = compute_T();
      T_ave/=1e-9*(X * Y * Z);
      if(((t != 0 && t%I == 0) || t==N) && (comm.me==0)) {
        double time = timer.seconds();
        printf("%i T=%lf Time (%lf %lf) (total, since_last_report) seconds\n",t,T_ave,time,time - old_time);
        old_time = time;
        if (t==N) validate(X,N,T_ave);
      }
    }
  }

  // Compute inner update
  struct ComputeInnerDT {};

  KOKKOS_FUNCTION
  void operator() (ComputeInnerDT, int x, int y, int z) const {
    double dT_xyz = 0.0;
    double T_xyz = T(x,y,z);
    dT_xyz += q * (T(x-1,y  ,z  ) - T_xyz);
    dT_xyz += q * (T(x+1,y  ,z  ) - T_xyz);
    dT_xyz += q * (T(x  ,y-1,z  ) - T_xyz);
    dT_xyz += q * (T(x  ,y+1,z  ) - T_xyz);
    dT_xyz += q * (T(x  ,y  ,z-1) - T_xyz);
    dT_xyz += q * (T(x  ,y  ,z+1) - T_xyz);

    dT(x,y,z) = dT_xyz;
  }

  void compute_inner_dT() {
    using policy_t = Kokkos::MDRangePolicy<Kokkos::Rank<3>,ComputeInnerDT,int>;
    int myX = T.extent(0);
    int myY = T.extent(1);
    int myZ = T.extent(2);
    Kokkos::parallel_for("ComputeInnerDT", 
      Kokkos::Experimental::require(policy_t(E_bulk,{1,1,1},{myX-1,myY-1,myZ-1}),
        Kokkos::Experimental::WorkItemProperty::HintLightWeight), *this);
  }

  // Compute non-exposed surface
  // Dispatch makes sure that we don't hit elements twice
  enum {left,right,down,up,front,back};

  template<int Surface>
  struct ComputeSurfaceDT {};

  template<int Surface>
  KOKKOS_FUNCTION
  void operator() (ComputeSurfaceDT<Surface>, int i, int j) const {
    int NX = T.extent(0);
    int NY = T.extent(1);
    int NZ = T.extent(2);
    int x, y, z;
    if(Surface == left)  { x = 0;    y = i;    z = j; }
    if(Surface == right) { x = NX-1; y = i;    z = j; }
    if(Surface == down)  { x = i;    y = 0;    z = j; }
    if(Surface == up)    { x = i;    y = NY-1; z = j; }
    if(Surface == front) { x = i;    y = j;    z = 0; }
    if(Surface == back)  { x = i;    y = j;    z = NZ-1; }

    double dT_xyz = 0.0;
    double T_xyz = T(x,y,z);

    // Heat conduction to inner body
    if(x > 0)    dT_xyz += q * (T(x-1,y  ,z  ) - T_xyz);
    if(x < NX-1) dT_xyz += q * (T(x+1,y  ,z  ) - T_xyz);
    if(y > 0)    dT_xyz += q * (T(x  ,y-1,z  ) - T_xyz);
    if(y < NY-1) dT_xyz += q * (T(x  ,y+1,z  ) - T_xyz);
    if(z > 0)    dT_xyz += q * (T(x  ,y  ,z-1) - T_xyz);
    if(z < NZ-1) dT_xyz += q * (T(x  ,y  ,z+1) - T_xyz);

    // Heat conduction with Halo    
    if(x == 0 && X_lo != 0)  dT_xyz += q * (T_left(y  ,z  ) - T_xyz);
    if(x == (NX-1) && X_hi != X)  dT_xyz += q * (T_right(y  ,z  ) - T_xyz);
    if(y == 0 && Y_lo != 0)  dT_xyz += q * (T_down(x  ,z  ) - T_xyz);
    if(y == (NY-1) && Y_hi != Y)  dT_xyz += q * (T_up(x  ,z  ) - T_xyz);
    if(z == 0 && Z_lo != 0)  dT_xyz += q * (T_front(x  ,y  ) - T_xyz);
    if(z == (NZ-1) && Z_hi != Z)  dT_xyz += q * (T_back(x  ,y  ) - T_xyz);

    // Incoming Power
    if(x == 0 && X_lo == 0) dT_xyz += P;

    // thermal radiation
    int num_surfaces = ( (x==0  && X_lo == 0) ? 1 : 0)
                      +( (x==(NX-1) && X_hi == X) ? 1 : 0)
                      +( (y==0  && Y_lo == 0) ? 1 : 0)
                      +( (y==(NY-1) && Y_hi == Y) ? 1 : 0)
                      +( (z==0  && Z_lo == 0) ? 1 : 0)
                      +( (z==(NZ-1) && Z_hi == Z) ? 1 : 0);
    dT_xyz -= sigma * T_xyz * T_xyz * T_xyz * T_xyz * num_surfaces;
    dT(x,y,z) = dT_xyz;
  }

  void pack_T_halo() {
    if(X_lo != 0)
      Kokkos::deep_copy(E_left, T_left_out ,Kokkos::subview(T,0,Kokkos::ALL,Kokkos::ALL));
    if(Y_lo != 0)
      Kokkos::deep_copy(E_down, T_down_out ,Kokkos::subview(T,Kokkos::ALL,0,Kokkos::ALL));
    if(Z_lo != 0) 
      Kokkos::deep_copy(E_front,T_front_out,Kokkos::subview(T,Kokkos::ALL,Kokkos::ALL,0));
    if(X_hi != X)
      Kokkos::deep_copy(E_right,T_right_out,Kokkos::subview(T,X_hi-X_lo-1,Kokkos::ALL,Kokkos::ALL));
    if(Y_hi != Y)
      Kokkos::deep_copy(E_up   ,T_up_out,   Kokkos::subview(T,Kokkos::ALL,Y_hi-Y_lo-1,Kokkos::ALL));
    if(Z_hi != Z) 
      Kokkos::deep_copy(E_back, T_back_out, Kokkos::subview(T,Kokkos::ALL,Kokkos::ALL,Z_hi-Z_lo-1));
  }

  void exchange_T_halo() {
/* This example provides two different algorithms for synchronizing the halo 
 * exchange that vary in the UPC++ constructs used. In both, a fence is called
 * on the Execution Spaces to ensure that the outgoing halos have been copied
 * to their buffer.
 * The default algorithm uses a upcxx::promise that becomes 
 * dependent the completion of each rput operation. These dependencies are 
 * encapsulated in a future object that is waited on in compute_surfaces_dT().
 * This means that all rputs initiated by the rank have completed, but only
 * once the subsequent barrier is reached is there certainty that its incoming
 * halos have been recieved.
 * Instead of a promise, the alternative algorithm uses an RPC as its 
 * completion object which increments a local counter variable. Completion
 * of the remote put operations here is determined by whether the counter has
 * been incremented to equal the number of neighbors the calling rank has in
 * the domain, and user-level progress is advanced until that happens, after
 * which the counter is reset.
 */ 
#if USE_HALO_BARRIER
    upcxx::promise<> p;
    if(X_lo != 0) {
      E_left.fence();
      upcxx::rput(T_left_out.data(),gptr_left,T_left_out.size(),
        upcxx::operation_cx::as_promise(p));
    }
    if(Y_lo != 0) {
      E_down.fence();
      upcxx::rput(T_down_out.data(),gptr_down,T_down_out.size(),
        upcxx::operation_cx::as_promise(p));
    }
    if(Z_lo != 0) { 
      E_front.fence();
      upcxx::rput(T_front_out.data(),gptr_front,T_front_out.size(),
        upcxx::operation_cx::as_promise(p));
    }
    if(X_hi != X) {
      E_right.fence();
      upcxx::rput(T_right_out.data(),gptr_right,T_right_out.size(),
        upcxx::operation_cx::as_promise(p));
    }
    if(Y_hi != Y) {
      E_up.fence();
      upcxx::rput(T_up_out.data(),gptr_up,T_up_out.size(),
        upcxx::operation_cx::as_promise(p));
    }
    if(Z_hi != Z) {
      E_back.fence();
      upcxx::rput(T_back_out.data(),gptr_back,T_back_out.size(),
        upcxx::operation_cx::as_promise(p));
    }
    fut_all = p.finalize();
#else
    if(X_lo != 0) {
      E_left.fence();
      upcxx::rput(T_left_out.data(),gptr_left,T_left_out.size(),
        upcxx::remote_cx::as_rpc([](){count++;}));
    }
    if(Y_lo != 0) {
      E_down.fence();
      upcxx::rput(T_down_out.data(),gptr_down,T_down_out.size(),
        upcxx::remote_cx::as_rpc([](){count++;}));
    }
    if(Z_lo != 0) { 
      E_front.fence();
      upcxx::rput(T_front_out.data(),gptr_front,T_front_out.size(),
        upcxx::remote_cx::as_rpc([](){count++;}));
    }
    if(X_hi != X) {
      E_right.fence();
      upcxx::rput(T_right_out.data(),gptr_right,T_right_out.size(),
        upcxx::remote_cx::as_rpc([](){count++;}));
    }
    if(Y_hi != Y) {
      E_up.fence();
      upcxx::rput(T_up_out.data(),gptr_up,T_up_out.size(),
        upcxx::remote_cx::as_rpc([](){count++;}));
    }
    if(Z_hi != Z) {
      E_back.fence();
      upcxx::rput(T_back_out.data(),gptr_back,T_back_out.size(),
        upcxx::remote_cx::as_rpc([](){count++;}));
    }
#endif
  }

  void compute_surface_dT() {
    using policy_left_t =  Kokkos::MDRangePolicy<Kokkos::Rank<2>,ComputeSurfaceDT<left>,int>;
    using policy_right_t = Kokkos::MDRangePolicy<Kokkos::Rank<2>,ComputeSurfaceDT<right>,int>;
    using policy_down_t =  Kokkos::MDRangePolicy<Kokkos::Rank<2>,ComputeSurfaceDT<down>,int>;
    using policy_up_t =    Kokkos::MDRangePolicy<Kokkos::Rank<2>,ComputeSurfaceDT<up>,int>;
    using policy_front_t = Kokkos::MDRangePolicy<Kokkos::Rank<2>,ComputeSurfaceDT<front>,int>;
    using policy_back_t =  Kokkos::MDRangePolicy<Kokkos::Rank<2>,ComputeSurfaceDT<back>,int>;

    int NX = T.extent(0);
    int NY = T.extent(1);
    int NZ = T.extent(2);

#if USE_HALO_BARRIER
    fut_all.wait();
    upcxx::barrier(comm.team);
#else
    while(count<nbors) upcxx::progress();
    count = 0;
#endif

    Kokkos::parallel_for("ComputeSurfaceDT_Left" , 
      Kokkos::Experimental::require(policy_left_t (E_left, {0,0},{NY,NZ}),
        Kokkos::Experimental::WorkItemProperty::HintLightWeight), *this);
    Kokkos::parallel_for("ComputeSurfaceDT_Right" , 
      Kokkos::Experimental::require(policy_right_t (E_right, {0,0},{NY,NZ}),
        Kokkos::Experimental::WorkItemProperty::HintLightWeight), *this);
    Kokkos::parallel_for("ComputeSurfaceDT_Down" , 
      Kokkos::Experimental::require(policy_down_t (E_down, {1,0},{NX-1,NZ}),
        Kokkos::Experimental::WorkItemProperty::HintLightWeight), *this);
    Kokkos::parallel_for("ComputeSurfaceDT_Up" , 
      Kokkos::Experimental::require(policy_up_t (E_up, {1,0},{NX-1,NZ}),
        Kokkos::Experimental::WorkItemProperty::HintLightWeight), *this);
    Kokkos::parallel_for("ComputeSurfaceDT_Front" , 
      Kokkos::Experimental::require(policy_front_t (E_front, {1,1},{NX-1,NY-1}),
        Kokkos::Experimental::WorkItemProperty::HintLightWeight), *this);
    Kokkos::parallel_for("ComputeSurfaceDT_Back" , 
      Kokkos::Experimental::require(policy_back_t (E_back, {1,1},{NX-1,NY-1}),
        Kokkos::Experimental::WorkItemProperty::HintLightWeight), *this);
  }

  // Some compilers have deduction issues if this were just a tagged operator
  // So did a full Functor here instead
  struct ComputeT {
    Kokkos::View<double***> T, dT;
    double dt;
    ComputeT(Kokkos::View<double***> T_, Kokkos::View<double***> dT_, double dt_):T(T_),dT(dT_),dt(dt_){}
    KOKKOS_FUNCTION
    void operator() (int x, int y, int z, double& sum_T) const {
      sum_T += T(x,y,z);
      T(x,y,z) += dt * dT(x,y,z);
    }
  };

  double compute_T() {
    using policy_t = Kokkos::MDRangePolicy<Kokkos::Rank<3>,Kokkos::IndexType<int>>;
    int X = T.extent(0);
    int Y = T.extent(1);
    int Z = T.extent(2);
    double my_T;
    Kokkos::parallel_reduce("ComputeT", Kokkos::Experimental::require(
        policy_t(E_bulk,{0,0,0},{X,Y,Z}),Kokkos::Experimental::WorkItemProperty
        ::HintLightWeight), ComputeT(T,dT,dt), my_T);
  /* This reduction has the added benefit of preventing a write-after-read 
   * hazard because all ranks wait to receive the result before moving to the
   * next iteration, meaning they have all consumed this iteration's halo data
   */
    return upcxx::reduce_all(my_T,upcxx::op_fast_add,comm.team).wait();
  }
};

int main(int argc, char* argv[]) {
  upcxx::init();
  Kokkos::initialize(argc,argv);
  {
    System sys(upcxx::world());
    if(sys.check_args(argc,argv))
      sys.timestep();
    sys.destroy_exec_spaces();
  }

  Kokkos::finalize();
  upcxx::finalize();
}
