# Laplace 2D

This directory contains an example illustrating how to efficiently solve
Laplace's equation in two-dimensions exploiting UPC++ distributed objects 
and asynchrony.

* This example uses only the host CPU for computation
* Each process exposes its four boundaries via `dist_object`
* _Signaling-puts_ are used to inform target processes of data availability
* _Relaxation method_ is used to compute the next state of the solution

To execute the program, at least two command-line arguments must be provided:

* `max_iters`: Maximum number of iterations to run
* `npts_x`: Width of the computational domain (number of points)

Optional:

* `npts_y`: Height of the computational domain (number of points)
* `np_x`: Number of subdomains along the horizontal axis
* `np_y`: Number of subdomains along the vertical axis
