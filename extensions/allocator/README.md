# UPC++ STL Allocator #

This header implements the Allocator concept for the UPC++ shared heap,
allowing STL and other C++ code to allocate memory in the registered segment.

This allows you to declare STL objects with types like:

```
std::vector<double,upcxx::extras::allocator<double>> my_vector;
```

and have the backing storage reside in the UPC++ shared heap.

For a more extensive example, see [allocator-example.cpp](allocator-example.cpp).

Note that because the storage for objects using this allocator reside
in the UPC++ shared heap, they may only be created and used in the 
interval between the `upcxx::init()` and `upcxx::finalize()` calls
that initialize and de-initialize the library, and within the same such
interval, for any given object.
This further implies that such objects may not have static storage
duration, since their creation would precede UPC++ library initialization.

Further note: the placement of the elements in shared memory doesn't magically
make `std::vector` or other containers aware of `global_ptr` and memory mappings
- for example, the local pointer to the elements embedded in the `std::vector`
should not be directly used by a different process in `local_team()`, because
the shared heap may be mapped into a different base address in virtual memory
on the non-allocating process. This means that naively using the
element-accessors on a `std::vector` created by a different process is still
likely to generate incorrect results. The correct way to handle this in the
case of a vector is demonstrated in the example program - namely, the owning
process can up-cast the vector's `data()` pointer to a `global_ptr` using
`try_global_ptr()` (generating a portable, universal pointer). That `global_ptr`
can then be communicated to other processes who can use the `global_ptr` for
RMA on the elements, or (in the case of co-located processes in `local_team()`)
downcast it using `global_ptr::local()` to a raw pointer to the shared elements
that is meaningful to that process.

