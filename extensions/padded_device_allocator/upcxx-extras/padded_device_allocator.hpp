// Author: Max Grossman
// Copyright 2022, The Regents of the University of California
// Terms of use are as specified in LICENSE.txt

#include <iostream>

#ifndef USE_UPCXX
  #if __CUDACC__ 
    // a bug in cudafe++ affects compilation of UPC++ headers up to 2019.9.0
    // for details, see issue 274
    #define USE_UPCXX 0
  #else 
    #define USE_UPCXX 1
  #endif
#endif

#if USE_UPCXX
#include <upcxx/upcxx.hpp>
#endif

#if __CUDACC__ || UPCXX_KIND_CUDA
  #include <cuda.h>
  #include <cuda_runtime_api.h>
#endif

namespace upcxx { namespace extras {

/*
 * Utility for estimating the memory usage of a pitched allocation with the
 * provided dimensions. This estimate includes only the memory usage of the
 * allocation itself: whatever bytes are necessary to allocate a width x height
 * array with each row of length width aligned to the estimated pitch. This does
 * not include any additional padding done by the UPC++ allocator. This utility
 * is useful for sizing CUDA memory segments when initializing a CUDA allocator.
 */
template <typename T>
std::size_t estimate_pitched_memory_usage(std::size_t width,
        std::size_t height, std::size_t estimated_device_pitch = 512) {
    size_t width_in_bytes = width * sizeof(T);
    size_t padding = (width_in_bytes % estimated_device_pitch == 0 ? 0 :
            estimated_device_pitch - (width_in_bytes % estimated_device_pitch));
    size_t padded_width = width_in_bytes + padding;
    return height * padded_width;
}

/*
 * Utility for computing the offset of element (row, col) in a 2D
 * pitched array with base address ptr. This variant of index_pitched is
 * appropriate for use within a CUDA kernel when directly interacting
 * with device pointers.
 */
template <typename T, typename index_type>
#ifdef __CUDA_ARCH__
__host__ __device__
#endif
inline T *index_pitched(T *ptr, index_type row, index_type col,
        index_type pitch) {
    static_assert(std::is_integral<index_type>::value && sizeof(index_type) > 1,
            "Index type to index_pitched must be integral and at least 16-bit");
    return  (T*)((char*)ptr + row * pitch) + col;
}

/*
 * Compute the offset in bytes from the start of a pitched allocation of a given
 * element specified by (row, col).
 */
template <typename T, typename index_type>
#ifdef __CUDA_ARCH__
__host__ __device__
#endif
inline index_type byte_offset_pitched(index_type row, index_type col,
        index_type pitch) {
    static_assert(std::is_integral<index_type>::value && sizeof(index_type) > 1,
            "Index type to byte_offset_pitched must be integral and at least 16-bit");
    return row * pitch + col * sizeof(T);
}

/*
 * Same as the above, but for upcxx::global_ptr.
 */
#if USE_UPCXX
template <typename T, typename index_type, upcxx::memory_kind kind>
upcxx::global_ptr<T,kind> index_pitched(
        upcxx::global_ptr<T,kind> ptr,
        index_type row, index_type col, index_type pitch) {
    static_assert(std::is_integral<index_type>::value && sizeof(index_type) > 1,
            "Index type to index_pitched must be integral and at least 16-bit");
    upcxx::global_ptr<uint8_t,kind> cptr =
        upcxx::reinterpret_pointer_cast<uint8_t>(ptr);
    cptr = (cptr + (row * pitch));
    return upcxx::reinterpret_pointer_cast<T>(cptr) + col;
}

namespace detail {
  template <typename Device>
  inline std::size_t default_device_pitch() {
    UPCXX_ASSERT_ALWAYS(false, "no default pitch for unknown device type");
  }

  #if UPCXX_KIND_CUDA // this should be the only device-dependent code
  template<>
  inline std::size_t default_device_pitch<upcxx::cuda_device>() {
    static size_t pitch = 0;
    if (!pitch) {
      void *ptr;
      cudaError_t err = cudaMallocPitch(&ptr, &pitch, 1, 2);
      UPCXX_ASSERT_ALWAYS(err == cudaSuccess);
      err = cudaFree(ptr);
      UPCXX_ASSERT_ALWAYS(err == cudaSuccess);
    }
    return pitch;
  }
  #endif
  #if UPCXX_KIND_HIP // this should be the only device-dependent code
  template<>
  inline std::size_t default_device_pitch<upcxx::hip_device>() {
    // As of ROCm 4.0-5.0, hipMallocPitch() is documented as pitching at 128-byte alignment:
    // https://github.com/RadeonOpenCompute/ROCm/raw/rocm-4.0.0/HIP-API_Guide_v4.0.pdf
    return 128;
  }
  #endif
} // namespace detail

template <typename Device>
class padded_device_allocator {
  protected:
    device_allocator<Device> &device_alloc; // wrapped allocator
    std::size_t device_pitch; // current state

    public:
      // pitched object constructor
      // Precondition: device_allocator.is_active() and reamins active (and unmoved)
        padded_device_allocator(device_allocator<Device> &device_allocator, 
            std::size_t pitch = 0) : device_alloc(device_allocator) {
          UPCXX_ASSERT_ALWAYS(device_allocator.is_active(), "padded_device_allocator "
            "constructed with an inactive device allocator");
          if (!pitch) pitch = detail::default_device_pitch<Device>();
          device_pitch = pitch;
        }
        // convenience accessors
        device_allocator<Device> &get_allocator() { return device_alloc; }
        operator device_allocator<Device>&() { return device_alloc; }

        // "clones" of all the specified device_allocator members (except constructors)
        using device_type = Device;
        static constexpr upcxx::memory_kind kind = Device::kind;

        /*
         * Equivalent to cudaMallocPitch, but with width and height expressed in
         * terms of T rather than in terms of bytes. The value returned in
         * out_pitch is still in terms of bytes.
         */
        template<typename T>
        upcxx::global_ptr<T,kind> allocate_pitched(std::size_t width, std::size_t height,
                                                   std::size_t &pitch) {
            size_t width_in_bytes = width * sizeof(T);
            size_t padding = (width_in_bytes % device_pitch == 0 ? 0 :
                    device_pitch - (width_in_bytes % device_pitch));
            size_t padded_width = width_in_bytes + padding;

            pitch = padded_width;

            upcxx::global_ptr<uint8_t, kind> mem;
#if UPCXX_SPEC_VERSION >= 20190301
            mem = device_alloc.template allocate<uint8_t>(height * padded_width, device_pitch);
#else
            /*
             * 512 is hard-coded here as an upper bound of the natural alignment
             * of CUDA global memory. This workaround is needed for any versions
             * of UPC++ prior to the resolution of UPC++ spec issue #145.
             */
            UPCXX_ASSERT(device_pitch <= 512);
            mem = device_alloc.template allocate<uint8_t, 512>(height * padded_width);
#endif
            return upcxx::reinterpret_pointer_cast<T>(mem);
        }

        // all non-static members of device_allocator are "pass-thru" wrappers
        template <typename T>
        global_ptr<T,kind> allocate(std::size_t n = 1, 
                                    std::size_t align = Device::template default_alignment<T>()) {
          return device_alloc.template allocate<T>(n, align);
        }

        template <typename T>
        void deallocate(global_ptr<T,kind> p) {
          return device_alloc.deallocate(p);
        }

        template <typename T>
        global_ptr<T,kind> to_global_ptr(typename Device::template pointer<T> p) const {
          return device_alloc.to_global_ptr(p);
        }

        template <typename T>
        static typename Device::id_type device_id(global_ptr<T, kind> gp) {
          return device_allocator<Device>::device_id(gp);
        }

        template <typename T>
        static typename Device::template pointer<T> local(global_ptr<T, kind> gp) {
          return device_allocator<Device>::local(gp);
        }

#if UPCXX_VERSION >= 20210905
        template <typename T>
        void deallocate(global_ptr<T,memory_kind::any> p) {
          return device_alloc.deallocate(p);
        }

        typename Device::id_type device_id() const {
          return device_alloc.device_id();
        }
#endif
};
#endif

}} // namespace upcxx::extras

