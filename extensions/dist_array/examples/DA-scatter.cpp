#include <fstream>
#include "upcxx-extras/dist_array.hpp"

using namespace upcxx;

int main(int argc, char *argv[]) {
    size_t BS=0;
    if (argc>1) {
        BS = atol(argv[1]);
        if (argc>2) {
            std::cerr << "Usage: upcxx-run -n [RANKS] ./DA-scatter [block size]" << std::endl;
            std::terminate();
        }
    }
    init();
    constexpr size_t N = 1000;
    extras::dist_array<double> my_dist_array(N,BS);
    barrier();
    if (!rank_me()) {
        std::ifstream input("examples/input");
        static double data_to_distribute[N];
        for (size_t i = 0; i < N; i++)
            input >> data_to_distribute[i];
        if (!BS)
            BS = my_dist_array.block_size();
        std::cout << "BS=" << BS <<std::endl;
        size_t block_id = 0;
        promise<> p;
        // iterate over blocks
        for (auto it = my_dist_array.bbegin(); it < my_dist_array.bend(); it++, block_id++) {
            // query size of current block: BS except for possible partial final block
            std::size_t block_size = it.block_size(); 
            future<global_ptr<double>> f_gptr = *it;
            f_gptr.then([=,&p](global_ptr<double> gptr) { // write to each block
               rput(&data_to_distribute[BS*block_id], 
                              gptr, block_size, operation_cx::as_promise(p));
            });
        }
        p.finalize().wait();
        block_id = 0;
        bool passed = true;
        double *buffer = new double[BS];
        for (auto it = my_dist_array.bbegin(); it < my_dist_array.bend(); it++, block_id++) {
            std::size_t block_size = it.block_size(); 
            rget(it->wait(), buffer, block_size).wait();
            for (size_t i = 0; i < block_size; i++) {
                if (data_to_distribute[block_id*BS+i] != buffer[i]) {
                    passed = false;
                    std::cout << "ERROR: scattered data doesn't match " 
                        "expected value" << std::endl;
                    break;       
                }            
            }
        }
        delete [] buffer;
        if (passed)
            std::cout << "SUCCESS" << std::endl;
    }
    barrier();
    my_dist_array.destroy(); // optional but recommended
    finalize();
}
