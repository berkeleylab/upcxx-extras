#include <random>
#include "upcxx-extras/dist_array.hpp"

using namespace upcxx;

int main(int argc, char *argv[]) {
    constexpr size_t  N = 64;
    constexpr size_t BS = 4;
    if (argc>1) {
        std::cerr << "Usage: upcxx-run -n [RANKS] ./DA-basic" << std::endl;
        std::terminate();
    }

    init();
    extras::dist_array<double>     DA(N, BS);
    extras::dist_array<double, BS> DA2(N); 
    // DA2 is equivalent to DA, but indexing may be optimized better

    // Element-wise global indexing into the dist array: (indexing overhead on each access)

    std::default_random_engine generator;
    std::uniform_int_distribution<> dist(0,N-1);
    size_t idx = dist(generator);
    future<double> f = DA.ptr(idx).then([](global_ptr<double> gptr)
        { return rget(gptr); });
    double d = f.wait();

    // different ways to efficiently iterate over the local slice:

    size_t check = 0;
    // .. by element across local_view local iterators
    for (double &e : DA.process_view()) {  e = 1; check++; }

    // .. by element indexing from the local base pointer
    for (size_t i = 0; i < DA.process_size(); i++) { DA.data()[i] = 1; check--; }

    if (check) {
        std::cerr << "ERROR: local element iteration differs for process_view "
            "and data accessing" << std::endl;
        std::terminate();
    }

    // .. by block stepping up from the local base pointer
    upcxx_assert_msg(!(DA.global_size()%DA.block_size()), "dist_array shouldn't have "
            "trailing block");
    double *pblock = DA.data();
    for (size_t b = 0; b < DA.process_blocks(); b++) {
        double *pend = pblock + DA.block_size();
        std::fill(pblock, pend, b);
        pblock = pend;
    }
    global_ptr<double> gptr;
    // .. by block with a global block iterator
    for (auto block_iter = DA.bbegin() + DA.team().rank_me();
         block_iter < DA.bend(); block_iter += DA.team().rank_n()) {
         gptr = block_iter->wait();
         std::fill(gptr.local(), gptr.local() + block_iter.block_size(), 42);
    }
    if (pblock != gptr.local()+DA.block_size()) {
        std::cerr << "ERROR: iteration by base pointer and block iterator "
            "doesn't match" << std::endl;
        std::terminate();
    }
    barrier();
    if (!rank_me()) std::cout << "SUCCESS" << std::endl;
    finalize();
}
