# dist\_array

## Overview

`upcxx::extras::dist_array` is an extension library implementing a distributed
shared array abstraction over UPC++. It supports convenient 1-D blocking of
array elements, in a style deliberately based upon 
[UPC shared arrays](https://upc.lbl.gov/publications/upc-spec-1.3.pdf). 
Unlike UPC, it also fully supports construction over subset teams, and can use
either a statically or dynamically specified block size (where the former enables
some marginal optimizations in indexing arithmetic overheads).

It is implemented over `upcxx::dist_object`, which is used to store and retrieve
the base pointer of each rank's slice of the distributed array elements.
It is designed to transparently support a variety of policies for maintaining the
directory of metadata referencing these base pointers, trading metadata scalability
for access time. 

In keeping with UPC++'s design philosophies, all communication remains syntactically
explicit and defaults to asynchronous behavior. Accordingly, the "access" methods 
provided by this class template do not directly perform communication on an array data
element of type T.  Rather, they return a `future<global_ptr<T>>` which eventually
provides a `global_ptr<T>` referencing the requested element, for use in regular
UPC++ RMA operations (ie `rget`, `rput`).  Unlike most UPC++ asynchronous operations,
this future should usually be "ready" in the common case (whenever the metadata
for the target rank is already locally available), but allows client code that
wants to bound their metadata usage at scale to still overlap any potential latencies
associated with metadata fetches.

This extension is still a work-in-progress, and all aspects of the interface
are subject to change, without notice.

## Specification
```C++
#include "upcxx-extras/dist_array.hpp"
// template parameters specify the type of object being distributed and how
// many logically contiguous elements are physically contiguous in a block
// the default value means that pure blocking is used
static constexpr size_t DYNAMIC_BLOCKED = (size_t)-1;
static constexpr size_t PURE_BLOCKED = (size_t)0;
template<typename T, size_t BS = DYNAMIC_BLOCKED, bool thread_safe = false>
class dist_array {
public:

class cache;
class info;
class process_array;

using dist_array<T,BS>::element_type = T;
using dist_array<T,BS>::value_type = future<global_ptr<T>>;
using dist_array<T,BS>::iterator = elemiter;
using dist_array<T,BS>::block_iterator = blociter;
using dist_array<T,BS>::reverse_iterator = elemriter;
using dist_array<T,BS>::reverse_block_iterator = blocriter;

//////////////////////////////////////////////////////////////////
// Construction

// allocate and construct a dist_array of given size/blocking
// collective over team and must be invoked by master persona, but NOT a barrier
// all of the arguments must be single-valued
// block_size specifies the distribution of elements across ranks
// positive integer block_size requests that many elements per block
// block_size of PURE_BLOCKED requests "pure blocking", which computes the blocksize value
//   that distributes elements as evenly as possible across ranks, as:
//     block_size = (count + team.rank_n() - 1) / team.rank_n()
// if block_size and BS are both DYNAMIC_BLOCKED, it is treated as PURE_BLOCKED.
// if block_size and BS specify different values and one is DYNAMIC_BLOCKED,
//   then the other value is used. If they differ otherwise, it is an error.
// optional final parameter controls cache size and replacement method
// the class is MoveConstructible, but not Copyable or DefaultConstructible
dist_array(size_t count, size_t block_size=BS, 
    upcxx::team &team = upcxx::world(), 
    upcxx::extras::cache &cache = default_cache());

dist_array(const dist_array& da) = delete;

dist_array& operator=(dist_array da) = delete;

dist_array(dist_array&& da) = default;

//////////////////////////////////////////////////////////////////
// Destruction

// Perform an entry barrier and then deallocate the dist_array
// Collective over team(), must be invoked by master persona and lev must be single-valued
// After the entry barrier specified by lev completes, 
//   or upon entry if lev == entry_barrier::none, any futures returned
//   by this object or its iterators must be in a ready state.
// Deallocates the element storage and places this object in a destroyed state,
//   after which the only operation that may be invoked is the C++ destructor.
void destroy(entry_barrier lev = entry_barrier::user);

// C++ destructor for dist_array object (non-collective)
// Either UPC++ must have been uninitialized since object construction,
//   or the object must have been destroyed by a prior call to destroy().
~dist_array();

//////////////////////////////////////////////////////////////////
// Global Indexing

// asynchronous indexing operation
// if cache has target rank's global_ptr, ready future is returned
// if cache doesn't have that, it's added (evicting old global_ptr if full)
// precondition: index <= global_size(), allowing for address of one-past element
//     in the event of a full final block, the one-past element will have
//     an address where the next block would start
future<global_ptr<T>> ptr(size_t index);

//////////////////////////////////////////////////////////////////
// Metadata Queries

// return the number of elements in a full block
// in both default and specified blocking schemes, the value is
//  consistent across ranks
size_t block_size() const;

// return # of local elements for specified rank
size_t process_size(intrank_t rank = team().rank_me()) const;

// return # of global elements
size_t global_size() const;

// return # of local blocks for specified rank
size_t process_blocks(intrank_t rank = team().rank_me()) const;

// return # of global blocks
size_t global_blocks() const;

// the dist_array's team
const upcxx::team& team() const;
upcxx::team& team();

//////////////////////////////////////////////////////////////////
// Local Indexing/Iterators

// returns pointer to this rank's data segment
T* data() const;

// produces a helper object with begin() and end() methods that behave like
//   standard C++ iterators over the local data (returning T*)
process_array process_view();

//////////////////////////////////////////////////////////////////
// Global Iterators

// 1st element of the dist_array
elemiter begin();

// 1st block of the dist_array
// iterator points to same address as begin()
blociter bbegin();

// where element after last one in dist_array would be
// if last block is full, this iterator's address won't be adjacent to last
// element
elemiter end();

// where block after last one in dist_array would be
// let rank with last block in entire dist_array be denoted R, then iterator
// returned here will point to address on rank (R+1)%team().rank_n()
blociter bend();

// last element of the dist_array
elemriter rbegin();

// beginning of last block in the dist_array
blocriter brbegin();

// where element before 1st one would be
// iterator points to address on rank team().rank_n()-1
elemriter rend();

// where block before 1st one would begin
// iterator points to address on rank team().rank_n()-1
blocriter brend();

//////////////////////////////////////////////////////////////////
// Indexing Info Queries

// use a global index to fill an Info struct
info global_idx_info(size_t global_idx);

// use a global block to fill an Info struct
// indices for the first element of the identified block will be returned
//   unless backward is true -- then the last element's indices are returned
info global_block_info(size_t global_block, bool backward = false);

// use a local index and rank to fill an Info struct
info process_idx_info(size_t local_idx, intrank_t rank = team().rank_me());

// use a local block and rank to fill an Info struct
// indices for the first element of the identified block will be returned
//   unless backward is true -- then the last element's indices are returned
info process_block_info(size_t local_block, intrank_t rank = team().rank_me(),
                      bool backward = false);
}
```

\newpage
### Class `cache`

```C++
// configure cache size and replacement method
// replace_method is an enum, currently with just a RAND value
cache(size_t size=world().rank_n(), replace_method replace=RAND)
```

### Class `info`

```C++
// only constructed within a [global/process]_[block/idx]_info function
// CopyAssignable and MoveAssignable

// Accessor members:
size_t global_idx();   // element index in global space
size_t global_block(); // block index in global space
size_t process_idx();    // element index in local slice
size_t process_block();  // block index in local slice
intrank_t rank();  // rank with affinity to referenced block
size_t phase(); // offset in elements from start of the containing block
```

### Class `process_array`

- nested class only constructed by `dist_array::process_view()` function
- useful for iterating over a `dist_array` rank's elements
- only has some container aliases and methods for forward process iteration by element, ie:
```C++
   using value_type = T;
   using reference = T&;
   using const_reference = const T&;
   using iterator = T*;
   using const_iterator = const T*;
   using difference_type = std::ptrdiff_t;
   using size_type = std::size_t;
   iterator begin(); // returns data()
   iterator end(); // returns data() + process_size()
   const_iterator cbegin();
   const_iterator cend();
   size_t size() const;
   bool empty() const;
```
- Note these return `[const] T*` and thus behave like standard C++ RandomAccessIterators
  over process memory (unlike the global iterators that add a level of `future` indirection
  over `global_ptr`).

### Block Size

- can be specified as the 2nd argument to either template or constructor
    - error if both options are used with unequal non-negative values
- the final global block will contain fewer elements than the rest if 
  `global_size() % block_size() != 0`
- if `global_size() < block_size() * team().rank_n()`, the highest-indexed
  ranks will not have any full blocks
- default is "pure blocking"
    - each rank that owns elements has one logically contiguous block
- specified block size
    - within a rank's data, elements which are physically `block_size()`
      apart are logically `block_size()*team().rank_n()` apart
    - lower ranks will have an extra block compared to higher ranks if
      `global_blocks() % team().rank_n() != 0`

### Thread safety

When the `thread_safe` parameter to the `dist_array` class template is false (the default),
the implementation is not thread-safe with respect to multi-threading within each rank, 
and the caller is responsible for ensuring that object construction and all
calls to member functions on a given `dist_array` or its associated iterators
are invoked from a unique `upcxx::persona` on each rank.  Otherwise, behavior is undefined.

When the `thread_safe` parameter to the `dist_array` class template is set to true, the
implementation is thread-safe and function calls from arbitrary personas/threads are permitted.
However operations documented as requiring the master persona maintain that requirement.

Note this thread-safety property applies only to the `dist_array` object and iterator sub-objects.
Thread-safety of accesses to the data elements stored in the array is always the client's responsibility.

### Caching Details

- contains `global_ptr`s to `dist_array` segments from other ranks
- each rank can have different contents
    - not a `dist_object`, but might be linked from one
- cache is modified when accessing from rank whose base `global_ptr` isn't stored
- when capacity >= nranks, fully populate in constructor using a gather-all collective
- when capacity < nranks, fetch on-demand
    - when cache is full, inserting new `global_ptr` requires evicting one entry
    - currently, evicted entry is chosen randomly

### Global Iterators

- prefix and postfix, forward and reverse, by-element and by-block
    - no const version
- same capabilities as RandomAccessIterator
    - exception: dereferencing returns `future<global_ptr<T>>`
    - example: `future<global_ptr<T>> operator++()`
    - comparison methods: ==, !=, <, >, <=, >=
    - assignment method: =
    - arithmetic operators: +, -
    - compound assignment operators: ++, --, +=, -=
        - for arithmetic and compound assignment operators, the unit of the
          iterator is the same as the other operand: adding an integer to an
          element iterator advances it by that many elements, while adding that
          same integer to a block iterator would advance it by that many blocks
        - subtracting an integer from an element iterator moves it backward by
          that many elements, while subtracting that same integer from a block
          iterator moves it back by that many blocks
    - iterator sum operator: iter + iter
        - iterator types must match and be over the same sequence
        - returns the iterators' combined difference from start of sequence in 
           their common unit type (blocks or elements) as an 
           `iterator::difference_type` (i.e. `std::ptrdiff_t`)
    - iterator difference operator: iter - iter
        - works analogously to C++ iterator difference 
        - iterator types must match and be over the same sequence
        - returns the difference between the iterators in their common
           unit type (blocks or elements) as an `iterator::difference_type` 
           (i.e. `std::ptrdiff_t`)
    - `operator->` and `operator*`: access `future<global_ptr>` referenced by current index
    - `operator[idx]`: access `future<global_ptr>` referenced by index idx
    - Unlike std:: iterators, `dist_array` iterators are guaranteed to permit
      arithmetic/assignment moving them past end-of-sequence (or before begin-of-sequence)
      corresponding to a position at least `team().rank_n()*block_size()` elements 
      "out-of-bounds" without overflow or error conditions.
      Dereferencing such out-of-bounds iterators is erroneous, but arithmetic and comparison
      operations continue to function as normal.
- block_size()
    - only available for forward and reverse block iterators
    - useful for not accessing beyond bounds of a trailing block

### Allocation details with partial or non-existent blocks

- When `global_size()` is a multiple of `block_size()` and 
  `global_blocks()` is a multiple of `team().rank_n()`,
  then every rank has exactly the same number of elements (there are no "ragged edges")
  and every rank allocates space for exactly the elements that "exist".
- When `global_size() % block_size() != 0` we have one trailing partial block
    - Normally allocate that trailing partial padded out to a full block size
      (wasting up to `sizeof(T) * (block_size() - 1)` bytes of space (on one rank)).
    - This is consistent with how UPC shared array implementations work, and
      ensures that blocks can always be safely RMA'd as full blocks by codes that
      ignore the possibility of partial blocks
         - Wasting some bytes on the wire for non-existent elements, but without risking a seg fault.
    - There is an exception for `block_size() > global_size()`, ensuring single-rank arrays
      never allocate more than `sizeof(T) *  global_size()` total element storage.
- When `global_blocks() % team().rank_n() != 0`, the number of blocks differs across ranks
    - Specifically, some ranks have `process_blocks() == floor(global_blocks()/team().rank_n())`
      and others have one additional block.
    - In the extreme case where `global_blocks() < team().rank_n()`, some ranks have zero blocks.
    - UPC shared array implementations generally waste space for these non-existent blocks 
      (at least for `global_blocks() > 1`), in order to maintain the symmetric heap.
    - `dist_array` does NOT allocate non-existent blocks for this case, because it has
      no such design constraint, and no compelling reason to waste that space.

## Examples

This section provides some simple motivating examples for `dist_array` and the
various features it offers.

### Example 0: Basic Usage


```C++
constexpr size_t N  = 1024*1024;
constexpr size_t BS = 1024;
upcxx::extras::dist_array<double>     DA(N, BS);
upcxx::extras::dist_array<double, BS> DA2(N); 
// DA2 is equivalent to DA, but indexing may be optimized better

// Element-wise global indexing into the dist array: (indexing overhead on each access)

size_t idx = rand() % N;
future<double> f = DA.ptr(idx).then([](global_ptr<double> gptr) { return rget(gptr); });
double d = f.wait();

// different ways to efficiently iterate over a rank's slice:

// .. by element across the process_view using local iterators
for (double &e : DA.process_view()) {  e =  1; }

// .. by element indexing from the process' base pointer
for (size_t i = 0; i < DA.process_size(); i++) { DA.data()[i] = 1; }

// .. by block stepping up from the rank's base pointer
double *pblock = DA.data();
for (size_t b = 0; b < DA.process_blocks(); b++) {
  double *pend = pblock + DA.block_size();
  std::fill(pblock, pend, b);
  pblock = pend;
}

// .. by block with a global block iterator
for (auto block_iter = DA.bbegin() + DA.team().rank_me();
     block_iter < DA.bend();
     block_iter += DA.team().rank_n()) {
         global_ptr<double> gptr = block_iter->wait();
         std::fill(gptr.local(), gptr.local() + DA.block_size(), 42);
}


```


### Example 1: File input to a `local_team` `dist_array` using Element Iterators


```C++
// alloc array over local team, arbitrary cyclic blocking
upcxx::extras::dist_array<double> my_input_data(N, 1, local_team());  
if (!local_team()::rank_me()) {
  // open the input file for this node
  std::fstream input_file("input", std::ios_base::in);
  for (future<global_ptr<double>> pelem : my_input_data) { // uses element iterators
    input_file >> *(pelem.wait().local());  // populate array elements
  }
}
upcxx::barrier();
```

This example shows file input into an array in shared space that will be later
used by all members of the `local_team`. One team representative opens the file
and reads the data into the `dist_array` using an element iterator and
local-memory-bypass. The use of a `dist_array` is mostly significant here because
it makes it easy to evenly distribute the elements between the shared heaps of
all the local team members, rather than the naive approach of allocating all
the space in the shared heap of one representative in the local team (which
creates imbalance in the shared heap occupancy of the ranks and has potentially
negative implications for NUMA affinity of the data). The same `dist_array` can
later (not shown) be used by team members to access the data -- using
element/block iterators, direct index (`ptr()`) operations, local slice access
(`data()`), etc. -- whatever is most appropriate to the access operation.

One cool property of this example is the code operating on the `dist_array` is
"layout oblivious": the code after construction need not know or care about the
data layout and works without modification for a `dist_array` of any block
size. The `dist_array` is self-describing about its own block size and this
seems a nice property for modularity, especially if `dist_array&` are ever
passed across an interface boundary.

Unfortunately, if a `dist_array` is declared with a static block size then this
information contaminates the type and even a "layout oblivious" callee needs a
compatible declaration to accept a `dist_array` (likely by templating
parametric over the block size). Assuming we add support for constructing
aliased views, this might motivate implicit aliasing conversions from
`dist_array<T,BS>` to `dist_array<T,DYNAMIC_BLOCKED>`, which would allow a callee to accept
and use a generic `dist_array<T>` without being templated parametric on `BS`
(but paying the additional computational indexing overheads of a dynamic
blocksize).


### Example 2: Block Scatter using Block Iterators


```C++
upcxx::extras::dist_array<double> my_dist_array(N,BS);
if (!rank_me()) {
  static double data_to_distribute[N]; 
  compute_data(data_to_distribute); // init the data, possibly from a file
  size_t block_id = 0;
  promise <> p;
  std::for_each(my_dist_array.bbegin(), my_dist_array.bend(),  // block iterators
                [=,&block_id](future<global_ptr<double>> f_gptr) {
                  f_gptr.then([=](global_ptr<double> gptr) { // write to each block
                     // note: this assumes N is an integer multiple of BS
                     rput(&data_to_distribute[block_id*BS], 
                          gptr, BS, operation_cx::as_promise(p));
                  });
                  block_id++;
                });
  p.finalize().wait();
}
upcxx::barrier();
```

This example shows creation of a job-wide distributed array, where one rank has
some data to scatter across the array. It uses a block iterator to populate
each block with a block-sized RMA. The use of a block iterator makes it easy to
express this algorithm, and potentially enables more efficient (incremental)
`dist_array` indexing arithmetic than one would induce with `global_blocks()`
separate calls to `ptr()`.


### Example 3: Disaggregated All-to-all Block Communication using Block Iterators


```C++
// alloc as (default) pure blocked, rank_n() elements per rank
upcxx::extras::dist_array<data_t> my_dist_array(rank_n() * rank_n());  
promise <> p;
intrank_t target_rank = 0;
std::for_each(my_dist_array.bbegin(), my_dist_array.bend(),  // block iterators
              [=,&target_rank](future<global_ptr<data_t>> f_gptr) {
                target_rank++;
                f_gptr.then([=](global_ptr<data_t> gptr) { // base pointer to each rank
                   data_t *contribution = compute_outgoing_data(target_rank);
                   rput(contribution, gptr + rank_me(), 1, operation_cx::as_promise(p));
                });
              });
p.finalize().wait();
upcxx::barrier();
// ... consume data in my local mailboxes
```

This code models a disaggregated all-to-all collective - this technique differs
from a traditional all-to-all collective because it computes outgoing data
segments overlapped with the communication of previous segments. Assume here
each rank needs to compute and send some bounded-size, specialized data to
every other rank. The `dist_array` is acting as a mailbox/landing-zone for
incoming data, allocated as pure-blocked with `rank_n()` elements (incoming
mailboxes) in every rank's block. 

### Example 4: 1-D Jacobi


```C++
long N = atol(argv[1]);
long iters = atol(argv[2]);
// construct data arrays, default to pure blocked
upcxx::extras::dist_array<double> old_DA(N), new_DA(N);
// find global index of where rank's (only) slice begins
size_t offset = old_DA.process_idx_info(0).global_idx();
site_t size_me = old_DA.process_size();
assert(size_me >= 2); // assume N >= ranks*2
global_ptr<double> old_left =  old_DA.ptr(offset-1).wait();
global_ptr<double> new_left =  new_DA.ptr(offset-1).wait();
global_ptr<double> old_right = old_DA.ptr(offset+size_me).wait();
global_ptr<double> new_right = new_DA.ptr(offset+size_me).wait();
for (double &e : old_DA.process_view()) { /* init data */ }
double *old_grid = old_DA.data();
double *new_grid = new_DA.data();
for (long it = 0; it < iters; it++) {
    upcxx::barrier();

    // start fetch of ghost cells
    future<double> left_ghost = rget(old_left);
    future<double> right_ghost = rget(old_right);

    // compute interior cells
    for (long i = 1; i < size_me-1; i++)
        new_grid[i] = 0.25 * (old_grid[i-1] + 2*old_grid[i] + old_grid[i+1]);

    // finish comms and compute boundary cells
    new_grid[0] = 0.25 * (left_ghost.wait() + 2*old_grid[0] + old_grid[1]);
    new_grid[size_me-1] = 0.25 * (old_grid[size_me-2] + 2*old_grid[size_me-1] 
                                                      + right_ghost.wait());
    // swap grids for next iteration
    std::swap(old_grid,new_grid);
    std::swap(old_left,new_left);
    std::swap(old_right,new_right);
}

```

This example implements a 1-D Jacobi method with two `dist_array`s, one for the
current iteration's data and one for the next iteration's data. They need
`global_ptr`s from their left and right neighbors to obtain ghost cell values 
for calculating the next iteration. Right after the two constructors, the `info`
helper class is used to determine the global indices of the ghost cells, which 
are then passed to `ptr` to determine their address. Because each rank can
determine its own number of elements through `process_size` and address neighboring
cells in the global index space, this code notably remains correct regardless
of whether `N` is a multiple of the number of ranks, without need for any
special-case logic to handle that corner case.

\newpage
## Open Design Questions

This section documents design decisions that are still being deliberated.

### Aliased Views

- One `dist_array` is used to produce another over the same team
  that aliases the same data array, with a possibly different blocksize
- Allows one to "view" the same data using global indexing with different
  blocksizes, as in UPC.
- Also enables type conversion of `dist_arrays` between static and dynamic
  blocked views, which may be useful for modularity.
- if the original `dist_array`'s block size is not a multiple of the new
  `dist_array`'s block size, global indexing on the new view may point 
  to some nonexistent elements.
- If we allow this, need to figure out data ownership semantics for views.
- Aliased views could all use the same underlying cache (as the base pointers
  don't change), but might also allow creation of views with an independent
  cache to enable phase-specific caching behavior.

#### Copy Constructor Option

- construct an aliased view over `dist_array` other using a different 
  blocksize -- collective over `other.team()`, but NOT a barrier

```C++
    template<ssize_t BS2>
    dist_array(dist_array<T, BS2> const &other, ssize_t block_size=BS); 
```

#### Factory Method Option

```C++
    template<ssize_t BS2=BS>
    dist_array<T,BS2> dist_array<T,BS>::alias(ssize_t block_size=BS2) const;
```

### Destroy Semantics

- destroy on either a per-view or per-data-array basis
- called collectively prior to `~dist_array`

#### Per-view

- decrements a reference count on the underlying data, freeing it once the
  count reaches zero

```C++
    template<class T, size_t BS>
    void dist_array<T,BS>::destroy_view();
```

#### Per-data-array

- frees underlying memory, silently invalidating any aliased views

```C++
    template<class T, size_t BS>
    void dist_array<T,BS>::destroy();
```

