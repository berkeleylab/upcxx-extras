# This Makefile demonstrates the recommended way to build simple UPC++ programs.
# Note this uses some GNU make extensions for conciseness.
#
# To use this makefile, set the UPCXX_INSTALL variable to the upcxx install directory, e.g.
# make UPCXX_INSTALL=<myinstalldir> hello-world
# or (for bash)
# export UPCXX_INSTALL=<myinstalldir>; make hello-world

ifeq ($(UPCXX_INSTALL),)
$(warning UPCXX_INSTALL environment variable is not set, assuming upcxx is in the PATH)
UPCXX=upcxx
UPCXXRUN=upcxx-run
else
UPCXX=$(UPCXX_INSTALL)/bin/upcxx
UPCXXRUN=$(UPCXX_INSTALL)/bin/upcxx-run
endif

# --------------------------------------------------------
#  Settings, with defaults:

UPCXX_THREADMODE ?= seq
export UPCXX_THREADMODE
UPCXX_CODEMODE ?= debug
export UPCXX_CODEMODE

EXTRA_FLAGS ?= 

PROCS ?= 4

# --------------------------------------------------------
# Program build logic

# Programs to build, assuming each has a corresponding *.cpp file
PROGRAMS = \
  hello-world \
  hello-world-rpc-to-0 \
  hello-world-file \
  hello-world-file-synchronized \
  dmap-insert-test \
  dmap-erase-update-test \
  drmap-insert-test \
  pi-no-overlap \
  pi-no-barrier \
  pi-conjoined \
  jac1d

all: $(PROGRAMS)

# The rule for building any example.
%: %.cpp $(wildcard *.h) $(wildcard *.hpp)
	$(UPCXX) $@.cpp -Wall $(EXTRA_FLAGS) -o $@

# --------------------------------------------------------
# Everything below is convenience targets for usability

OUTPUT = output.txt

# runs all the programs currently built in the working directory
run:
	@for f in $(PROGRAMS) ; do \
	  if test -x $$f ; then \
	    rm -f $(OUTPUT) ; \
	    ( set -x ; $(UPCXXRUN) -n $(PROCS) ./$$f ; ) ; \
	    if test -f $(OUTPUT) ; then \
	      echo $(OUTPUT) : ; cat $(OUTPUT) ; \
	    fi ; \
	  fi ; \
	done

# builds and runs a particular test (eg `make run-ex0`)
run-%: % force
	-@rm -f $(OUTPUT)
	$(UPCXXRUN) -n $(PROCS) ./$<
	-@if test -f $(OUTPUT) ; then \
	     echo $(OUTPUT) : ; cat $(OUTPUT) ; \
	  fi

clean:
	rm -f $(PROGRAMS) $(OUTPUT)

force:

.PHONY: clean all run force

