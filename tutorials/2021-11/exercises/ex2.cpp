// Simple 1-d Jacobi solver tutorial example
// Dan Bonachea and Amir Kamil
// NOTE: This code is written in naive/simple ways to demonstrate
// various library features for pedagogical purposes in tutorials.
// This is NOT intended to be a high-performance solver.

#include <upcxx/upcxx.hpp>

#include <iostream>
#include <tuple>
#include <random>
#include <cstdlib>
#include <cmath>
#undef NDEBUG
#include <cassert>

// main local array data structure
upcxx::global_ptr<double> old_grid_gptr, new_grid_gptr;
double *old_grid, *new_grid;

upcxx::intrank_t left, right; // ranks of my neighbors

// direct pointers to neighbor grids
upcxx::global_ptr<double> left_old_grid, left_new_grid;
upcxx::global_ptr<double> right_old_grid, right_new_grid;

// ---------------------------------------------------------------------------
// Bootstrapping: Exchange of global array pointers with neighbors

// bootstrap left gptrs using rpc and futures
upcxx::future<> bootstrap_left() {
  upcxx::future<upcxx::global_ptr<double>, upcxx::global_ptr<double>> fut =
      upcxx::rpc(left, []() {
        return upcxx::make_future(old_grid_gptr, new_grid_gptr);
      });
  return fut.then(
    [](upcxx::global_ptr<double> ptr1, upcxx::global_ptr<double> ptr2) {
      left_old_grid = ptr1;
      left_new_grid = ptr2;
    });
}

// bootstrap right gptrs using dist_object
void bootstrap_right() {
  //======================================================================
  // EXERCISE:
  //
  // Use dist_object to retrieve the values of `old_grid_gptr` and `new_grid_gptr`
  // from the neighbor process with rank `right` and store them into the variables
  // `right_old_grid` and `right_new_grid` declared above.
  //
  using upcxx::dist_object;
  using upcxx::global_ptr;
  // FILL IN CODE HERE:



  // HINT: you'll want to construct at least one upcxx::dist_object from the values
  // to be shared and then call the dist_object::fetch(rank_id) method

  //======================================================================
  upcxx::barrier(); // ensures dobj is not destructed until all ranks have it
}

// ---------------------------------------------------------------------------
// job parameters
long global_domain_sz = 1024; // total domain size
long iters = 100; // iterations to compute
long grid_size; // per-process array size
long local_domain_sz;

// ---------------------------------------------------------------------------
// permanent setup: establish main datastructures and metadata
void setup() {
  // allocate grid in shared space
  old_grid_gptr = upcxx::new_array<double>(grid_size);
  new_grid_gptr = upcxx::new_array<double>(grid_size);

  // downcast to raw C++ pointers to our grid
  old_grid = old_grid_gptr.local();
  new_grid = new_grid_gptr.local();

  left = (upcxx::rank_me() + upcxx::rank_n() - 1) % upcxx::rank_n();
  right = (upcxx::rank_me() + 1) % upcxx::rank_n();

  upcxx::barrier(); // ensure local metadata established

  // exchange remote info needed by some algorithms
  bootstrap_left().wait();
  bootstrap_right();

  upcxx::barrier();
}


// check invariants
void sanity_check() {
  upcxx::barrier();
  assert(old_grid_gptr && new_grid_gptr);
  assert(old_grid && new_grid);
  assert(old_grid == old_grid_gptr.local());
  assert(new_grid == new_grid_gptr.local());
  assert((left + 1) % upcxx::rank_n() == upcxx::rank_me());
  assert((upcxx::rank_me() + 1) % upcxx::rank_n() == right);
  assert(left_old_grid && left_new_grid);
  assert(left_old_grid.where() == left && left_new_grid.where() == left);
  assert(right_old_grid && right_new_grid);
  assert(right_old_grid.where() == right && right_new_grid.where() == right);
  upcxx::barrier();
}

// setup grid for each test
void init_grid() {
  // init to uniform pseudo-random distribution, independent of job size
  long baseidx = upcxx::rank_me() * local_domain_sz;
  std::mt19937_64 rgen(1);  rgen.discard(baseidx);
  for (long i = 1; i < grid_size - 1; ++i) {
    old_grid[i] = rgen() % 100;
  }
  // clear ghost cells and other grid points
  old_grid[0] = std::nan("1");
  old_grid[grid_size - 1] = std::nan("2");
  for (long i = 0; i < grid_size; ++i) {
    new_grid[i] = std::nan("3");
  }
}

// check for correct answer
bool check_grid() {
  double sum = 0;
  for (long i = 1; i < grid_size - 1; ++i) {
    sum += old_grid[i];
  }
  sum = upcxx::reduce_all(sum, upcxx::op_fast_add).wait();

  // check known results for global_domain_sz x iters
  static struct { long sz, iters; double correct; } known_answers[] = {
                {    8192,   100,       408097 },
                {    4096,   100,       207466 },
                {    2048,   100,       103965 },
                {    1024,   100,        51900 },
                {     512,   100,        26021 },
                {     256,   100,        12596 },
                {     128,   100,         6129 },
                {      64,   100,         2943 },
                {      32,   100,         1502 },
                {      16,   100,          760 },
                {       8,   100,          352 },
                {       4,   100,          166 },
  };

  const char *result = "(unknown problem size)";
  bool success = true;
  for (auto &answer : known_answers) {
    if (global_domain_sz == answer.sz
        /* && iters == answer.iters */ // iteration count does not mathematically affect sum
        ) {
      if (std::fabs(sum - answer.correct) < 0.001)
           result = "PASSED";
      else { result = "FAILED"; success = false; }
      break;
    }
  }

  if (!upcxx::rank_me()) std::cout << "  result = " << sum << "  " << result << std::endl;
  return success;
}

double get_cell(long i) {
  return old_grid[i];
}

// ---------------------------------------------------------------------------
// Simple RMA get version
// this version communicates with RMA get and synchronizes with barrier
void jacobi_rget() {
  for (long it = 0; it < iters; it++) {
    upcxx::future<double> left_ghost  = upcxx::rget(left_old_grid+grid_size-2);
    upcxx::future<double> right_ghost = upcxx::rget(right_old_grid+1);

    // compute interior cells
    for (long i = 2; i < grid_size - 2; ++i)
      new_grid[i] = 0.25 * (old_grid[i-1] + 2*old_grid[i] + old_grid[i+1]);

    // consume fetched values to compute boundary
    new_grid[1] = 0.25 *
      (left_ghost.wait() + 2*old_grid[1] + old_grid[2]);
    new_grid[grid_size-2] = 0.25 *
      (old_grid[grid_size-3] + 2*old_grid[grid_size-2] + right_ghost.wait());

    upcxx::barrier(); // ensure incoming rgets serviced before next iter overwrites

    // setup next iteration
    std::swap(old_grid, new_grid);
    std::swap(left_old_grid, left_new_grid);
    std::swap(right_old_grid, right_new_grid);
  } // iteration loop
}
// ---------------------------------------------------------------------------
// Simple RMA put version
// this version communicates with RMA put and synchronizes with barrier
void jacobi_rput() {
  for (long it = 0; it < iters; it++) {
    // push to neighboring ghost cells
    upcxx::future<> puts = upcxx::when_all(
                      upcxx::rput(old_grid[1], left_old_grid+grid_size-1),
                      upcxx::rput(old_grid[grid_size-2], right_old_grid)  );

    // compute interior cells
    for (long i = 2; i < grid_size - 2; ++i)
      new_grid[i] = 0.25 * (old_grid[i-1] + 2*old_grid[i] + old_grid[i+1]);

    puts.wait();
    upcxx::barrier(); // ensure incoming puts have arrived

    // consume arrived values to compute boundary
    new_grid[1] = 0.25 *
      (old_grid[0] + 2*old_grid[1] + old_grid[2]);
    new_grid[grid_size-2] = 0.25 *
      (old_grid[grid_size-3] + 2*old_grid[grid_size-2] + old_grid[grid_size-1]);

    // setup next iteration
    std::swap(old_grid, new_grid);
    std::swap(left_old_grid, left_new_grid);
    std::swap(right_old_grid, right_new_grid);
  } // iteration loop
}
// ---------------------------------------------------------------------------

// NOTE: Other Jacobi communication variants available in examples/jac1d.cpp

// ---------------------------------------------------------------------------
// test driver
// Usage: upcxx-run -np PROCS jac1d [iters] [global_domain_sz]
int main(int argc, char *argv[]) {
  upcxx::init();

  if (argc > 1) iters = std::atol(argv[1]);
  if (argc > 2) global_domain_sz = std::atol(argv[2]);

  if (!upcxx::rank_me()) std::cout << "Running 1-d Jacobi on " << upcxx::rank_n()
                                   <<  " procs, iters=" << iters << " global_domain_sz="
                                   << global_domain_sz << std::endl;

  local_domain_sz = global_domain_sz / upcxx::rank_n();
  grid_size = local_domain_sz + 2; // 2 ghost cells per process

  if (!upcxx::rank_me()) { // check job parameters
    if (iters % 2) {
      std::cerr << "iters must be even" << std::endl;
      abort();
    }
    if (global_domain_sz != local_domain_sz * upcxx::rank_n()) {
      std::cerr << "Num ranks must evenly divide global_domain_sz" << std::endl;
      abort();
    }
    if (local_domain_sz < 3) {
      std::cerr << "global_domain_sz must be provide at least 3 grid points per process" << std::endl;
      abort();
    }
  }

  upcxx::barrier();

  setup(); // init permanent data structures

  // main test driver
  struct { void (*fp)(void); const char *desc; } variants[] = {
         { jacobi_rget,          "jacobi_rget" },
         { jacobi_rput,          "jacobi_rput" },
  };
  bool success = true;
  for (auto & v : variants) {
    upcxx::barrier();
    sanity_check();
    init_grid();
    if (!upcxx::rank_me()) std::cout << "Running " << v.desc << "..." << std::endl;
    upcxx::barrier();
    v.fp();
    success &= check_grid();
    upcxx::barrier();
  }

  sanity_check();

  if (!upcxx::rank_me()) std::cout << (success?"SUCCESS":"FAILED") << std::endl;
  upcxx::finalize();
}
