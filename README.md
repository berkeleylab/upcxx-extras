# UPC\+\+ Extras: Examples and Extensions for UPC\+\+ #

This repository provides example codes and library extensions for UPC++.
The codes in this repository require the latest version of the
[UPC++ library](http://upcxx.lbl.gov).

UPC++ is a parallel programming library for developing C++ applications with
the Partitioned Global Address Space (PGAS) model.  UPC++ has three main
objectives:

* Provide an object-oriented PGAS programming model in the context of the
  popular C++ language

* Expose useful asynchronous parallel programming idioms unavailable in
  traditional SPMD models, such as remote function invocation and
  continuation-based operation completion, to support complex scientific
  applications
 
* Offer an easy on-ramp to PGAS programming through interoperability with other
  existing parallel programming systems (e.g., MPI, OpenMP, CUDA)

## Contents overview

### `examples/`
  
This directory contains various example codes using UPC++.
See the README file in each subdirectory for further details and instructions.

### `extensions/`

This directory contains library extensions to UPC++, written in terms of UPC++ APIs.
See the README file in each subdirectory for further details and instructions.

### `tutorials/`

This directory contains materials from UPC++ tutorial events.
See the README file in each subdirectory for further details.

## Legal terms

UPC++ Extras is an optional component of the [UPC++ library](http://upcxx.lbl.gov),
which is distributed separately for convenience of packaging. 
As such, this software is subject to the same copyright notice and licensing agreement, 
which is available in [LICENSE.txt](LICENSE.txt)

## Bug Reports and Feature Requests

Please use the 
[UPC++ Issue tracker](https://bitbucket.org/berkeleylab/upcxx/issues?status=new&amp;status=open)
with Component="upcxx-extras"

## ChangeLog

### 2023-12-18: d70172d

* Added a 2D heat diffusion solver in [examples/heat2d](examples/heat2d)
* Added materials from the [CUF23](https://go.lbl.gov/cuf23) tutorial in [tutorials/2023-07](tutorials/2023-07)
* Fixed issue #619: Invalid memory accesses in `dist-array/DA-scatter` example

### 2023-03-31: 5c62ed8

* Fixed issue #577: `kokkos_3dhalo` example crashes when using multiple HIP streams
* Fixed issue #578: `kokkos_3dhalo` example fails on multi-GPU nodes due to device mismatch
* Fixed issue #579: `kokkos_3dhalo` crashes for some job layouts due to device segment limit

### 2022-10-06: 34137dd

* Added a 2D laplace solver in [examples/laplace2d](examples/laplace2d)
* Modified `kokkos_3dhalo` example to use `gpu_default_device`, adding support for
  AMD ROCm/HIP GPUs with the UPC++ `hip_device` memory kind.
* `examples/vector_accumulate`: Minor fixes and clarifications

### 2022-03-31: c23b013

* `padded_cuda_allocator` changed to templated [`padded_device_allocator<Device>`](extensions/padded_device_allocator) 
  and constructed from `device_allocator`
* Fixed issue #537: Numerical error in kokkos-based 3d heat conduction examples

### 2021-10-26: 77bdd24

* Add materials from the Nov 2021 UPC++ SC21 tutorial in [tutorials/2021-11](tutorials/2021-11)
* Add UPC++/Kokkos interoperability example for Monte Carlo calculation of pi in [examples/kokkos_montecarlo](examples/kokkos_montecarlo)
* Add UPC++/Kokkos interoperability example for 3D heat simulation in [examples/kokkos_3dhalo](examples/kokkos_3dhalo),
  featured in this [PAW-ATM'21 paper](http://doi.org/10.25344/S4630V)

### 2020-11-09: a17a399

* Add materials from the Nov 2020 UPC++ SC20 tutorial in `tutorials`
* `dist_array`: Clarify type is MoveConstructible, but not Copyable or DefaultConstructible
* Fixed issue #381: `cannon_cuda` fails to validate with 16 ranks

### 2020-08-21: 5a3e7c3

* Fixed issue #365: `dist_array/DA-threads` example has a thread-safety bug
* Fixed issue #366: thread-safety bug in `dist_array::ptr`
* Fixed issue #326: `dist_array`: cache optimization for overlapped misses
* `dist_array`: Change return of `dist_array::info::rank()` to `intrank_t`
* Makefile improvements, especially for cannon and CUDA examples

### 2020-03-12: e62e5bc

* New [`dist_array` class template extension](extensions/dist_array) offers a prototype
  scalable distributed array facility, implemented atop `upcxx::dist_object`.
* All extension headers now live in a "upcxx-extras" subdirectory

### 2019-12-16: 9742d88

* Add materials from the Dec 2019 UPC++ tutorial in `tutorials`
* Update jac3d example to use pitched/padded allocations in UPC++ CUDA

### 2019-11-06: 3b74217

* Add materials from the Nov 2019 UPC++ tutorial in `tutorials`

### 2019-10-31: 525f095

* Add `padded_cuda_allocator` extension for row-padded CUDA memory allocation.
* Add [extend-add example](examples/extend-add), featured in the [IPDPS'19 paper](https://doi.org/10.25344/S4V88H)
* Update Jacobi 3D example to use `upcxx::copy` memory kinds API

### 2019-06-19: bfd492f

* Initial public version 

